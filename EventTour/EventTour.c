#include "..\common\common.h"
#include "EventTour.h"

HANDLE		hEvent			= NULL;
HANDLE      hThreadEvent    = NULL;
volatile	DWORD threads	= 0;

int main(int argc, CHAR* argv[])
{
	CHAR	title[MAX_PATH] = {0};
    DWORD   choice          = 0;

    hThreadEvent = CreateEvent(NULL, FALSE, TRUE, NULL);
	do
	{
		sprintf( title,   "Events Tour\n"
						  "Events Waiting: %d\n"
						  "\tBlack: Event not created\n"
						  "\tGreen: Event Created\n"
						  "\tBlue:  Thread Waiting on Event\n", threads);

		choice = ConsolePromptMenu( TRUE,
								    /*T*/ title,
									/*1*/ "Create Manual Event",
									/*2*/ "Create Auto Event",
									/*3*/ "Thread Wait Event",
									/*4*/ "Set Event",
									/*5*/ "Reset Event",
									/*6*/ "Pulse Event",
									/*7*/ "Close Event",
                                    NULL);
		switch(choice)
		{
			case 0:
				
			break;

			case EVENT_TOUR_CREATE_MANUAL_EVENT:
				if(hEvent != NULL)
				{
					ConsolePromptPressEnter("Event already Created\n");
					break;
				}
                /* TODO: Create manual, unamed, unset event */
				
				if(hEvent != NULL)
				{
					ConsoleColor(BACKGROUND_GREEN);
				}
			break;

			case EVENT_TOUR_CREATE_AUTO_EVENT:
				if(hEvent != NULL)
				{
					ConsolePromptPressEnter("Event already Created\n");
					break;
				}
                /* TODO: Create auto, unamed, unset event */
				
				if(hEvent != NULL)
					ConsoleColor(BACKGROUND_GREEN);
			break;

			case EVENT_TOUR_THREAD_WAIT:
				if(hEvent == NULL)
				{
					ConsolePromptPressEnter("Event not created yet\n");
					break;
				}
                CreateThread(NULL,0,EventThread,NULL,0,NULL);
			break;

			case EVENT_TOUR_SET_EVENT:
                /* TODO: Set hEvent */
				
			break;

			case EVENT_TOUR_PULSE_EVENT:
                /* TODO: Pulse hEvent */
				
			break;

			case EVENT_TOUR_RESET_EVENT:
                /* TODO: Reset hEvent */
				
			break;

			case EVENT_TOUR_CLOSE_EVENT:
                /* TODO: Close hEvent and set to NULL*/
				
                threads = 0;
				ConsoleColor(0);
			break;

			default:
				ConsolePromptPressEnter("\nInvalid Choice\n");
			break;
		}
        switch(WaitForSingleObject(hThreadEvent, 250))
        {
            case WAIT_OBJECT_0:

            break;
        }
	}while(choice!=0);
}

DWORD WINAPI EventThread( LPVOID lp)
{
	ConsoleColor(BACKGROUND_BLUE);

    /* TODO: Increment thread count */
	InterlockedIncrement(&threads);

    /* Don't worry about this */
    SetEvent(hThreadEvent);

    /* TODO: Wait for hEvent */
	
    /* TODO: Decrement thread count */
	

	if(threads == 0)
	{
		ConsoleColor(BACKGROUND_GREEN);
	}
	return 0;
}

