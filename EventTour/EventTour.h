#include <Windows.h>

#define EVENT_TOUR_CREATE_MANUAL_EVENT	1
#define EVENT_TOUR_CREATE_AUTO_EVENT	2
#define EVENT_TOUR_THREAD_WAIT			3
#define EVENT_TOUR_SET_EVENT			4
#define EVENT_TOUR_RESET_EVENT			5
#define	EVENT_TOUR_PULSE_EVENT			6
#define EVENT_TOUR_CLOSE_EVENT			7

DWORD WINAPI EventThread( LPVOID lp);