/* 
	Creates two anonymous pipe, one way pipes. One is for
	[1] Parent process to recieve redirection from client's stdout
	[2] Child  process to recieve redirection from parents stdin

	Pipe Handles Named in Reference to Parent STDIO

	     ----------------------------------------------------------------------------------------------------
	     |                       PARENT                                |PIPE|                      CHILD       |
	     ----------------------------------------------------------------------------------------------------
	     ==========================================================================================================
	     |                                                             |    |                                     |
	---> | ReadConsole(hStdIn)  --->  WriteFile(hPipeParentWrite) ---> |PIPE| --> hPipeChildRead --> ChildStdIn   | --->  
	     |                                                             |    |                                     | 
 CONSOLE ==========================================================================================================   CMD.EXE
	     |                                                             |    |                                     |
	<--- | WriteConsole(STD_OUTPUT) <--- ReadFile(hPipeParentRead) <-- |Pipe| <-- hPipeChildWrite <-- ChildStdout | <---
	     |                                                             |    |                                     |
	     ==========================================================================================================
*/
#include "..\common\common.h"
#include "Redirect.h"

HANDLE ghChildProcess = NULL;
HANDLE hStdIn = NULL; // Handle to parents std input.
BOOL bRunThread = TRUE;

int main (int argc, LPSTR argv [])
{
	HANDLE hPipeParentRead,hPipeChildWrite;
	HANDLE hPipeChildRead,hPipeParentWrite;
	HANDLE hErrorWrite;
	HANDLE hThread;
	DWORD ThreadId;
	SECURITY_ATTRIBUTES sa;

    /* TODO:
	   Set up the security attributes struct. This is needed
	   because otherwise the child process will not inherit the 
	   pipe handle
    */
	sa.nLength= sizeof(SECURITY_ATTRIBUTES);
	sa.lpSecurityDescriptor = NULL;
	sa.bInheritHandle = TRUE;

	// Create the child output pipe.
	if (!CreatePipe(&hPipeParentRead,&hPipeChildWrite,&sa,0))
		ReportError("CreatePipe", 1, TRUE);

	// Create the child input pipe.
	if (!CreatePipe(&hPipeChildRead,&hPipeParentWrite,&sa,0))
		ReportError("CreatePipe", 1, TRUE);


	// Create a duplicate of the output write handle for the std error
	// write handle. This is necessary in case the child application
	// closes one of its std output handles.
	if (!DuplicateHandle(GetCurrentProcess(),hPipeChildWrite,
                         GetCurrentProcess(),&hErrorWrite,0,
                         TRUE,DUPLICATE_SAME_ACCESS))
		ReportError("DuplicateHandle", 1, TRUE);

	
	// The parent's read and write side of the pipe should not be inherited 
	SetHandleInformation(hPipeParentWrite, HANDLE_FLAG_INHERIT, 0);
	SetHandleInformation(hPipeParentRead , HANDLE_FLAG_INHERIT, 0);

	// Get std input handle so you can close it and force the ReadFile to
	// fail when you want the input thread to exit.
	if ( (hStdIn = GetStdHandle(STD_INPUT_HANDLE)) == INVALID_HANDLE_VALUE )
		ReportError("GetStdHandle", 1, TRUE);

	PrepAndLaunchRedirectedChild(hPipeChildWrite,hPipeChildRead,hErrorWrite);


	// Close pipe handles (do not continue to modify the parent).
	// You need to make sure that no handles to the write end of the
	// output pipe are maintained in this process or else the pipe will
	// not close when the child process exits and the ReadFile will hang.
	if (!CloseHandle(hPipeChildWrite)) ReportError("CloseHandle", 1, TRUE);
	if (!CloseHandle(hPipeChildRead))  ReportError("CloseHandle", 1, TRUE);
	if (!CloseHandle(hErrorWrite))     ReportError("CloseHandle", 1, TRUE);


	// Launch the thread that gets the input and sends it to the child.
	hThread = CreateThread(NULL,
						   0,
						   GetAndSendInputThread,
                           (LPVOID)hPipeParentWrite,
						   0,
						   &ThreadId);
	if (hThread == NULL)  ReportError("CreateThread", 1, TRUE);


	// Read the child's output.
	// This call blocks until redirection is complete
	ReadAndHandleOutput(hPipeParentRead);
    // Redirection is complete


	// Force the read on the input to return by closing the stdin handle.
	if (!CloseHandle(hStdIn))  ReportError("CloseHandle", 1, TRUE);


	// Tell the thread to exit and wait for thread to die.
	bRunThread = FALSE;

	if (WaitForSingleObject(hThread,INFINITE) == WAIT_FAILED)
		ReportError("WaitForSingleObject", 1, TRUE);

	if (!CloseHandle(hPipeParentRead))  ReportError("CloseHandle", 1, TRUE);
	if (!CloseHandle(hPipeParentWrite))  ReportError("CloseHandle", 1, TRUE);
}


/////////////////////////////////////////////////////////////////////// 
// PrepAndLaunchRedirectedChild
// Sets up STARTUPINFO structure, and launches redirected child.
/////////////////////////////////////////////////////////////////////// 
void PrepAndLaunchRedirectedChild(HANDLE hChildStdOut,
                                  HANDLE hChildStdIn,
                                  HANDLE hChildStdErr)
{
	PROCESS_INFORMATION pi;
	STARTUPINFO si;
	TCHAR szCmdline[] = {"c:\\windows\\system32\\cmd.exe"};

	// Set up the start up info struct.
	ZeroMemory(&pi, sizeof(pi) );
	ZeroMemory(&si,sizeof(si));
	si.cb = sizeof(si);
	si.dwFlags = STARTF_USESTDHANDLES;
	si.hStdOutput = hChildStdOut;
	si.hStdInput  = hChildStdIn;
	si.hStdError  = hChildStdErr;

	// Use these if you want to hide the child:
	si.dwFlags |= STARTF_USESHOWWINDOW;
	si.wShowWindow = SW_HIDE;

    // Launch the process that you want to redirect
	if (!CreateProcess(NULL,
					   szCmdline,
					   NULL,
					   NULL,
					   TRUE,
                       CREATE_NEW_CONSOLE,
					   NULL,
					   NULL,
					   &si,
					   &pi))
		ReportError("CreateProcess", 1, TRUE);

    // Close any unnecessary handles.
	if (!CloseHandle(pi.hThread)) ReportError("CloseHandle", 1, TRUE);
	if (!CloseHandle(pi.hProcess)) ReportError("CloseHandle", 1, TRUE);

}


/////////////////////////////////////////////////////////////////////// 
// ReadAndHandleOutput
// Monitors handle for input. Exits when child exits or pipe breaks.
/////////////////////////////////////////////////////////////////////// 
void ReadAndHandleOutput(HANDLE hPipeRead)
{
    CHAR buf[MAX_PATH];
    DWORD nBytesRead;
    DWORD nCharsWritten;
	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);

    while(TRUE)
    {
        if (!ReadFile(hPipeRead,buf,sizeof(buf), &nBytesRead,NULL) || !nBytesRead)
        {
			if (GetLastError() == ERROR_BROKEN_PIPE)
				break; // pipe done - normal exit path.
			else
				ReportError("ReadFile", 1, TRUE); // Something bad happened.
        }

        // Display the character read on the screen.
        if (!WriteConsoleA(hStdOut ,buf,nBytesRead,&nCharsWritten,NULL))
            ReportError("WriteConsole", 1, TRUE);
    }
}


/////////////////////////////////////////////////////////////////////// 
// GetAndSendInputThread
// Thread procedure that monitors the console for input and sends input
// to the child process through the input pipe.
// This thread ends when the child application exits.
/////////////////////////////////////////////////////////////////////// 
DWORD WINAPI GetAndSendInputThread(LPVOID lp)
{
	CHAR buf[MAX_PATH] = {0};
    DWORD nBytesRead,nBytesWrote;
    HANDLE hPipeWrite = (HANDLE)lp;

    // Get input from our console and send it to child through the pipe.
    while (bRunThread)
    {
        if(!ReadConsoleA(hStdIn,buf,MAX_PATH,&nBytesRead,NULL))
            ReportError("ReadConsole", 1, TRUE);
		buf[nBytesRead] = 0;

        if (!WriteFile(hPipeWrite,&buf,nBytesRead,&nBytesWrote,NULL))
        {
        if (GetLastError() == ERROR_NO_DATA)
            break; // Pipe was closed (normal exit path).
        else
            ReportError("WriteFile", 1, TRUE);
        }
    }

    return 1;
}




