/* 
	Creates two anonymous pipe, one way pipes. One is for
	[1] Parent process to recieve redirection from client's stdout
	[2] Child  process to recieve redirection from parents stdin

	Pipe Handles Named in Reference to Parent STDIO

	     ----------------------------------------------------------------------------------------------------
	     |                       PARENT                                |PIPE|                      CHILD       |
	     ----------------------------------------------------------------------------------------------------
	     ==========================================================================================================
	     |                                                             |    |                                     |
	---> | ReadConsole(hStdIn)  --->  WriteFile(hPipeParentWrite) ---> |PIPE| --> hPipeChildRead --> ChildStdIn   | --->  
	     |                                                             |    |                                     | 
 CONSOLE ==========================================================================================================   CMD.EXE
	     |                                                             |    |                                     |
	<--- | WriteConsole(STD_OUTPUT) <--- ReadFile(hPipeParentRead) <-- |Pipe| <-- hPipeChildWrite <-- ChildStdout | <---
	     |                                                             |    |                                     |
	     ==========================================================================================================
*/

#include <Windows.h>

void ReadAndHandleOutput(HANDLE hPipeRead);
void PrepAndLaunchRedirectedChild(HANDLE hChildStdOut,
                                  HANDLE hChildStdIn,
                                  HANDLE hChildStdErr);
DWORD WINAPI GetAndSendInputThread(LPVOID lpvThreadParam);
