#ifndef _FILEDIRMGMT_HANDLES
#define _FILEDIRMGMT_HANDLES

#include <Windows.h>

#define FILEDIRMGMT_CHANGE_DIR   1
#define FILEDIRMGMT_MAKE_DIR     2
#define FILEDIRMGMT_DELETE_DIR   3
#define FILEDIRMGMT_LIST_DIR     4

BOOL FileDirMgmtGetCurrentDirectory(LPSTR dir);
BOOL FileDirMgmtChangeDirectory();
BOOL FileDirMgmtMakeDirectory();
BOOL FileDirMgmtDeleteDirectory();
BOOL FileDirMgmtListDirectory(LPSTR dir, BOOL echoOnSuccess);

#endif