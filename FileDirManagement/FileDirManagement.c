#include "../common/common.h"
#include "FileDirManagement.h"

int main(int argc, CHAR* argv[])
{
    DWORD   choice              = INVALID_MENU_ENTRY;
    CHAR    curDir[MAX_PATH+12] = {0};
    CHAR    msg[MAX_PATH+2]     = {0};
    
    do
    {
        FileDirMgmtGetCurrentDirectory(curDir);
        sprintf_s(msg,MAX_PATH,"%s\n\n",curDir);
        choice = ConsolePromptMenu( TRUE,  // Clear Screen
                                    msg,     // Title
                                    "Change Directory",        // Option 1
                                    "Make Directory",          // Option 2
                                    "Delete Directory",        // Option 3
                                    "List Directory",          // Option 4
                                    NULL);                     // Must be NULL
        
        switch(choice)
        {
            // Finished
            case 0:
                 
            break;

            // Options
            case FILEDIRMGMT_CHANGE_DIR:
                FileDirMgmtChangeDirectory();
            break;

            case FILEDIRMGMT_MAKE_DIR:
               FileDirMgmtMakeDirectory();
            break;

            case FILEDIRMGMT_DELETE_DIR:
                FileDirMgmtDeleteDirectory();
            break;

            case FILEDIRMGMT_LIST_DIR:
                FileDirMgmtListDirectory(curDir, TRUE);
            break;

            // Unknown
            case INVALID_MENU_ENTRY:
            default:
                ConsolePromptPressEnter("Invalid Choice");
            break;
        }
    }while(choice != 0);

    return 0;
}

BOOL FileDirMgmtGetCurrentDirectory(LPSTR dir)
{
    /* TODO: Get Current Directory and return it in dir
             Return TRUE on success, FALSE on failure 
    */
	if (!GetCurrentDirectory(MAX_PATH + 12, dir))
	{
		PrintMsg(stdout, "Error getting current dir: %d\n", GetLastError());
		return FALSE;
	}
	
	return TRUE;
}

BOOL FileDirMgmtChangeDirectory()
{
    CHAR dir[MAX_PATH] = {0};

    if(!FileDirMgmtGetCurrentDirectory(dir))
        return FALSE;
    printf("\n");
    FileDirMgmtListDirectory(dir, FALSE);

    ConsolePrompt(
        "Enter Directory To Change To: ",
        dir,
        MAX_PATH,
        TRUE);

    /* TODO: Set The Current Directory using the 
             input returned from user: dir. 
             Return FALSE if failure
    */
	if (!SetCurrentDirectory(dir))
	{
		PrintMsg("Error setting directory: %d\n", GetLastError());
		return FALSE;
	}

    ConsolePromptPressEnter("SUCCESS");
    return TRUE;
}


BOOL FileDirMgmtMakeDirectory()
{
    CHAR dir[MAX_PATH] = {0};

    if(!FileDirMgmtGetCurrentDirectory(dir))
        return FALSE;
    printf("\n");
    FileDirMgmtListDirectory(dir, FALSE);

    ConsolePrompt(
        "Enter Directory To Create: ",
        dir,
        MAX_PATH,
        TRUE);

    /* TODO: Create The Directory using the
    input returned from user: dir.
    Return  FALSE on failure
    */
	if (!CreateDirectory(dir, NULL))
	{
		PrintMsg("Error creating Dir: %d\n", GetLastError());
		return FALSE;
	}

    ConsolePromptPressEnter("SUCCESS");
    return TRUE;
}

/*  
    Currently only deletes an empty file 
    Deleting a folder with files would require deleting
    all the files first and then deleting the folder
    or using SHFileOperation from shell32.lib
*/
BOOL FileDirMgmtDeleteDirectory()
{
    CHAR dir[MAX_PATH] = {0};

    if(!FileDirMgmtGetCurrentDirectory(dir))
        return FALSE;
    printf("\n");
    FileDirMgmtListDirectory(dir, FALSE);

    ConsolePrompt(
        "Enter Directory To Delete: ",
        dir,
        MAX_PATH,
        TRUE);

    /* TODO: Remove The  Directory using the
    input returned from user: dir.
    Return  FALSE on failure
    */
	if (!RemoveDirectory(dir))
	{
		PrintMsg("Failed to delete directory %s: %d", dir, GetLastError());
		return FALSE;
	}

    ConsolePromptPressEnter("SUCCESS");
    return TRUE;
}

BOOL FileDirMgmtListDirectory(LPSTR dir, BOOL echoOnSuccess)
{
    WIN32_FIND_DATA     FindFileData;
	HANDLE              hFind                 = INVALID_HANDLE_VALUE;
    CHAR                searchDir[MAX_PATH+2] = {0};

    /* TODO:
        make sure that your path ends with a backslash, append one
        assuming there is room for one 
    */
	DWORD searchDirLen = strlen(searchDir) + 1;
	if (searchDirLen > MAX_PATH + 2)
	{
		ConsolePromptPressEnter("Directory length too long\n");
		return FALSE;
	}

	searchDir[searchDirLen] = '\\';
    sprintf_s(searchDir, MAX_PATH, "%s\\*", dir);

	// TODO: Find the first file in the directory.
	hFind = FindFirstFile(searchDir, &FindFileData);

    /* example of error reporting if Finding First file failed*/
	if (hFind == INVALID_HANDLE_VALUE) 
	{
		ReportError("FindFirstFile Error", 0, TRUE);
        ConsolePromptPressEnter(NULL);
		return FALSE;
	}

	do
	{
        
        //if (REPLACE_ME/* TODO: Is the file a directory? How do you check a flag in a bit field?*/)
        if (FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			printf("[DIR ] %s\n", FindFileData.cFileName);
		}
        else
        {
			printf("[FILE] %s\n", FindFileData.cFileName);
        }
	} while (FindNextFile(hFind, &FindFileData));

    /* TODO: Close the Find File Handle... careful to do it right here */
	FindClose(hFind);

	
    if(echoOnSuccess)
        ConsolePromptPressEnter(NULL);

	return TRUE;
}