#include "..\common\common.h"

static BOOL WINAPI Handler(DWORD cntrlEvent);

/* set this flag when you want the program to exit */
BOOL exitFlag = FALSE;

DWORD main(DWORD argc, LPSTR argv[])
{
    // Set Console Control Handler to Handler()
	if (!SetConsoleCtrlHandler(Handler, TRUE))
	{
		ReportError("Error setting handler", 1, TRUE);
	}

    /* Periodically print a message until Control-C is pressed */
    while(!exitFlag)
    {
        printf("Press Control-C to get rid of this message\n");
        Sleep(1000);
    }

    ConsolePromptPressEnter("Handled Ctrl-C Successfully\n");
    return 0;
}

BOOL WINAPI Handler(DWORD cntrlEvent)
{
    // correctly identify the correct cases and handle them
	switch (cntrlEvent) 
	{ 
		/* CTRL-C */
		case CTRL_C_EVENT:
			printf("CTRL-C Captured\n");
			exitFlag = TRUE;
			return TRUE;
		break;

        /* close event */
		case CTRL_CLOSE_EVENT:
			printf("Close Captured\n");
			return TRUE;
		break;

        /* CTRL-BREAK */
		case CTRL_BREAK_EVENT:
			printf("CTRL-Break Captured\n");
			return TRUE;
		break;

		default:
			return FALSE;
		break;
	}
}