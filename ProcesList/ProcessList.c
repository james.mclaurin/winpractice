// ProcessList.cpp : Defines the entry point for the console application.
//

#include "../common/common.h"

/* TODO: Create typedefs for the following functions:
    EnumProcesses 
    GetModuleBaseName 
    EnumProcessModules 
*/

/* TODO: Create global variables to function pointers of each function type above */

void PrintProcessNameAndID( DWORD processID )
{
	CHAR szProcessName[MAX_PATH] = "<unknown>";
	
	// Get a handle to the process.
	HANDLE hProcess = OpenProcess( PROCESS_QUERY_INFORMATION |PROCESS_VM_READ,
                                   FALSE, 
                                   processID );
    if(hProcess == NULL)
    {
        return;
    }

    // Get the process name.
    if (NULL != hProcess )
	{
		HMODULE hMod;
		DWORD cbNeeded;

        /* TODO: replace void* with your EnumProcessModules ptr*/
		if ((void*)( hProcess, &hMod, sizeof(hMod), &cbNeeded) )
		{
            /* TODO: replace void* with you GetModuleBaseName */
			(void*)( hProcess, 
                               hMod, 
                               szProcessName, 
							   sizeof(szProcessName) );
		}
        
	}

	// Print the process name and identifier
    printf( "%-6u   %-s\n",processID, szProcessName );
    CloseHandle( hProcess );
}

void main( )
{

    // Get the list of process identifiers.
    DWORD   aProcesses[1024]    = {0};
    DWORD   cbNeeded            = 0;
    DWORD   cProcesses          = 0;
    DWORD   i                   = 0;                      

    /* TODO:
       For each of your global functions use the functon GetProcAddressWrapper
       to load the functions from psapi.dll. You don't need to create the function
       GetProcAddressWrapper again, it is in common.lib
    */
	
    /* TODO: Validate functions loaded */

    /* TODO: Replace void* with your pointer to EnumProcess */
	if ( !(void*)( aProcesses, sizeof(aProcesses), &cbNeeded ) )
		return;

	// Calculate how many process identifiers were returned.
	cProcesses = cbNeeded / sizeof(DWORD);

    // Print the name and process identifier for each process. 
	for ( i = 0; i < cProcesses; i++ )
	{
		if( aProcesses[i] != 0 )
		{
			PrintProcessNameAndID( aProcesses[i] );
		}
	}

}