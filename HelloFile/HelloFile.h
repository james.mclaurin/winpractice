#ifndef _HELLO_FILE
#define _HELLO_FILE

#include <Windows.h>

#define HELLO_SET_FILENAME    1
#define HELLO_CREATE_FILE     2
#define HELLO_OPEN_FILE       3
#define HELLO_CLOSE_FILE      4
#define HELLO_WRITE_FILE      5
#define HELLO_READ_FILE       6
#define HELLO_DELETE_FILE     7

BOOL HelloSetFileName(LPSTR filename);
BOOL HelloCreateNewFile(LPSTR filename, PHANDLE phFile);
BOOL HelloOpenExistngFile(LPSTR filename, PHANDLE phFile);
BOOL HelloCloseFile(PHANDLE hFile);
BOOL HelloWriteFile(HANDLE hFile);
BOOL HelloReadFile(HANDLE hFile);
BOOL HelloDeleteFile(LPSTR filename);

#endif