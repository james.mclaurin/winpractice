#include "../common/common.h"
#include "HelloFile.h"

int main(int argc, CHAR* argv[])
{
    DWORD   choice              = INVALID_MENU_ENTRY;
    CHAR    filename[MAX_PATH]  = {0};
    HANDLE  hFile               = INVALID_HANDLE_VALUE;
    CHAR    msg[MAX_PATH*2]     = {0};

    do
    {

        memset(msg,0,sizeof(msg));
        sprintf(msg,"File Name   : %s\n"
                    "File Handle : 0x%02x\n\n" 
                    "Choose an Option:\n",
                    filename,
                    (unsigned int)hFile);

        choice = ConsolePromptMenu( TRUE,                   // Clear Screen
                                    msg,                    // Title
                                    "Set Filename",         // Option 1
                                    "Create New File",      // Option 2
                                    "Open Existing File",   // Option 3
                                    "Close File Handle",    // Option 4
                                    "Write To File",        // Option 5
                                    "Read From File",       // Option 6
                                    "Delete File",          // Option 7
                                    NULL);                  // Must be NULL
        switch(choice)
        {
            // Finished
            case 0:
                if(hFile != INVALID_HANDLE_VALUE)
                {
                    ConsolePromptPressEnter("Better Close That Handle\n");
                    choice = INVALID_MENU_ENTRY;
                    continue;
                }
            break;

            // Options
            case HELLO_SET_FILENAME:
                HelloSetFileName(filename);
            break;

            case HELLO_CREATE_FILE:
               HelloCreateNewFile(filename, &hFile);
            break;

            case HELLO_OPEN_FILE:
                HelloOpenExistngFile(filename, &hFile);
            break;

            case HELLO_CLOSE_FILE:
                HelloCloseFile(&hFile);
            break;

            case HELLO_WRITE_FILE:
                HelloWriteFile(hFile);
            break;

            case HELLO_READ_FILE:
                HelloReadFile(hFile);
            break;

            case HELLO_DELETE_FILE:
                HelloDeleteFile(filename);
            break;

            // Unknown
            case INVALID_MENU_ENTRY:
            default:
                ConsolePromptPressEnter("Invalid Choice");
            break;

        }
    }while(choice != 0);

    return 0;
}

// TODO: Use ConsolePrompt to get a filename from user
BOOL HelloSetFileName(LPSTR filename)
{
	ConsolePrompt("File Name: ", filename, MAX_PATH, TRUE);

	return TRUE;
}

/* TODO:
    - Create a file only if it doesn't already exist
    - Return success
    - if failure set HANDLE to INVALID_HANDLE_VALUE
*/
BOOL HelloCreateNewFile(LPSTR filename, PHANDLE phFile)
{
    BOOL    success = FALSE;
    HANDLE  hFile   = INVALID_HANDLE_VALUE;

    /* Validate phFile is valid : return FALSE otherwise */
	if (*phFile != INVALID_HANDLE_VALUE)
	{
		ConsolePromptPressEnter("Handle already Exists\n");
		return FALSE;
	}
    __try
    {
        /* Create a new file */
		hFile = CreateFile(
			filename,
			GENERIC_READ | GENERIC_WRITE,
			FILE_SHARE_READ | FILE_SHARE_WRITE,
			NULL,
			CREATE_ALWAYS,
			FILE_ATTRIBUTE_NORMAL,
			NULL
		);

        /* Check Success */
		if (hFile == INVALID_HANDLE_VALUE)
		{
			ConsolePromptPressEnter("Error Opening file\n");
			return FALSE;
		}
       
        /* SUCCESS */
        ConsolePromptPressEnter("SUCCESS\n");
        success = TRUE;
    }
    __finally
    {
        /* return hFile in phFile */
		*phFile = hFile;
    }

    return success;
}

/* TODO:
    - Open an existing file
    - Return success
    - if failure set HANDLE to INVALID_HANDLE_VALUE
*/
BOOL HelloOpenExistngFile(LPSTR filename, PHANDLE phFile)
{
    BOOL    success = FALSE;
    HANDLE  hFile   = INVALID_HANDLE_VALUE;

    /* validate phFile */
	if (*phFile != INVALID_HANDLE_VALUE)
	{
		ConsolePromptPressEnter("Handle already exists\n");
		return FALSE;
	}

    __try
    {
        /* Open Existing File */
		hFile = CreateFile(
			filename,
			GENERIC_READ | GENERIC_WRITE,
			FILE_SHARE_READ | FILE_SHARE_WRITE,
			NULL,
			OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL,
			NULL
		);

        /* Validate file handle */
		if (hFile == INVALID_HANDLE_VALUE)
		{
			ConsolePromptPressEnter("Error opening file\n");
			__leave;
		}

        ConsolePromptPressEnter("SUCCESS\n");
        success = TRUE;
    }
    __finally
    {
        /* return hFile in phFile */
		*phFile = hFile;
    }

    return success;
}

/* TODO:    
    Close File Handle
    set handle it to INVALID_HANDLE VALUE
   */
BOOL HelloCloseFile(PHANDLE phFile)
{
    /* validate phFile */
	if (*phFile == INVALID_HANDLE_VALUE) return FALSE;

    /* Close Handle */
	CloseHandle(*phFile);

    /* Set handle to INVALID_HANDLE_VALUE */
	*phFile = INVALID_HANDLE_VALUE;

    return TRUE;

}

/* TODO:
    Prompt User for Data to write to file
    Set file pointer to end of file
    Write that data to the file
*/
BOOL HelloWriteFile(HANDLE hFile)
{
    BOOL    success         = FALSE;
    CHAR    msg[1024]       = {0};
    DWORD   msgLen          = 0;
    DWORD   bytesWritten    = 0;

    __try
    {
        /* Prompt User for message to write to file */
		ConsolePrompt("Message to write:", msg, 1024, TRUE);

        /* What is the sring length */
		msgLen = strlen(msg);

        /* Set the file pointer to the end of the file */
		SetFilePointer(
			hFile,
			0,
			0,
			FILE_END);

        /* Write message to file */
		if (!WriteFile(hFile, msg, msgLen, &bytesWritten, NULL))
		{
			ConsolePromptPressEnter("Error Writing file\n");
			return FALSE;
		}

        ConsolePromptPressEnter("SUCCESS");
        success = TRUE;
    }
    __finally
    {
        if(!success)
        {
            ConsolePromptPressEnter(NULL);
        }
            
    }

    return success;
}

/* TODO
    Get FileSize (assume small file for this lab)
    malloc memory to store data
    Set File pointer to beginning of file
    Read the contents of the file and display to the screen
    clean up memory in __finally
*/
BOOL HelloReadFile(HANDLE hFile)
{
    BOOL    success     = FALSE;
    DWORD   filesize    = 0;
    DWORD   bytesRead   = 0;
    CHAR    *data       = NULL;

    __try
    {
        /* what is the file size, if it is 0 nothing to read*/
		filesize = GetFileSize(hFile, NULL);
		if (filesize == 0)
		{
			PrintMsg(stdout, "Nothing to read");
			return TRUE;
		}

        /* Allocate memory to data, add one byte so you can null terminate */
		data = (PCHAR)calloc(1, filesize + 1);

        /* Set File Pointer to beginning of file */
		SetFilePointer(
			hFile,
			0,
			NULL,
			FILE_BEGIN);

        /* Read the file into data buffer */
		ReadFile(
			hFile,
			data,
			filesize,
			&bytesRead,
			NULL);

        /* NULL terminate string for printing */
		//data[bytesRead + 1] = 0;

        ConsolePromptPressEnter(data);
        success = TRUE;
        
    }
    __finally
    {
        /* free data if it is valid */
		if (data != NULL) free(data);
    }

    return success;
}

/* Delete file */
BOOL HelloDeleteFile(LPSTR filename)
{
    /* Delete the file */
	DeleteFile(filename);

    ConsolePromptPressEnter("SUCCESS");
    return TRUE;
}
