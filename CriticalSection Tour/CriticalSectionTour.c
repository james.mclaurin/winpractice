#include "..\common\common.h"
#include "CriticalSectionTour.h"

#define ITERATIONS  50

CRITICAL_SECTION cs;

DWORD m = 0, n = 0;

DWORD WINAPI ExchangeIncrement(LPVOID lp);
DWORD WINAPI ExchangeIncrementSynchronized(LPVOID lp);
VOID SingleThreaded();
VOID MultiThreadedNoSynchronization();
VOID MultiThreadedWithSynchronization();

int main(int argc, CHAR* argv[])
{
    m = n = 0;
    SingleThreaded();

    m = n = 0;
    MultiThreadedNoSynchronization();

    m = n = 0;
    MultiThreadedWithSynchronization();

}

VOID SingleThreaded()
{
    DWORD i;
    printf("Single Thread Start:  m = %d : n = %d\n", m, n);
    for (i = 0; i < ITERATIONS; i++)
    {
        ExchangeIncrement(NULL);
    }
    printf("Single Thread Finish: m = %d : n = %d\n\n", m, n);
}

/* Running this program multiple times you should not see the same output
each time, and it won't match the single threaded output
*/
VOID MultiThreadedNoSynchronization()
{
    DWORD   i;
    HANDLE  hThreads[ITERATIONS];

    printf("Multi Thread No Sync Start:  m = %d : n = %d\n", m, n);
    for (i = 0; i < ITERATIONS; i++)
    {
        hThreads[i] = CreateThread(NULL, 0, ExchangeIncrement, NULL, 0, NULL);
    }
    WaitForMultipleObjects(ITERATIONS, hThreads, TRUE, INFINITE);
    printf("Multi Thread No Sync Finish: m = %d : n = %d\n\n", m, n);

}

VOID MultiThreadedWithSynchronization()
{
    DWORD   i;
    HANDLE  hThreads[ITERATIONS];

    /* TODO: Create your critical section */

    printf("Multi Thread With Sync Start:  m = %d : n = %d\n", m, n);
    for (i = 0; i < ITERATIONS; i++)
    {
        hThreads[i] = CreateThread(NULL, 0, ExchangeIncrementSynchronized, NULL, 0, NULL);
    }
    WaitForMultipleObjects(ITERATIONS, hThreads, TRUE, INFINITE);
    printf("Multi Thread With Sync Finish: m = %d : n = %d\n\n", m, n);

    /* TODO: Delete your critical Section*/

}

/* Think of this as a database pull adjust value and write back to database
n is your database
m is your local storage
you pull from the database
then you adjust the value
then return
there will be latency, this is why the sleep(100) calls are placed */
DWORD WINAPI ExchangeIncrement(LPVOID lp)
{
    m = n;
    Sleep(10);
    m = m + 1;
    Sleep(10);
    n = m;

    return 0;
}

DWORD WINAPI ExchangeIncrementSynchronized(LPVOID lp)
{
    /* TODO: Create the same functionality in ExchangeIncrement but
    have it protected with a Critical Section, this will
    allow you to have the same output as Single Threaded*/

    return 0;
}
