#include "ServicesTour.h"

int main(int argc, CHAR* argv[])
{
	DWORD   choice = 0;

	do
	{
		choice = ConsolePromptMenu(FALSE,
			/*T*/ "Services Tour Menu\n",
			/*1*/ "Print Service List:",
			/*2*/ "Create Service",
			/*3*/ "Start Service",
			/*4*/ "Stop Service",
			/*5*/ "Delete Service",
			NULL);
		switch (choice)
		{
		case 0:

			break;

		case PRINT_SERVICE_LIST:
			WspPrintServiceList();
			break;

		case CREATE_SERVICE:
			WspCreateService();
			break;

		case START_SERVICE:
			WspStartService();
			break;

		case STOP_SERVICE:
			WspStopService();
			break;

		case DELETE_SERVICE:
			WspDeleteService();
			break;

		default:
			printf("Invalid Choice");
			break;
		}

		ConsolePromptPressEnter(NULL);
		ClearScreen();
	} while (choice != 0);

	return 0;
}

VOID WspPrintServiceList()
{
	SC_HANDLE schSCManager;
	DWORD BytesNeeded = 0;
	DWORD svcRetCount = 0;
	LPDWORD resumeHandle = 0;
	char servicebuf[1024] = { 0 };

	// Enumerate Services variables
	SC_HANDLE scHandle = OpenSCManager(
		NULL,
		NULL,
		SC_MANAGER_ENUMERATE_SERVICE
	);
	if (NULL == scHandle)
	{
		ReportError("OpenSCManager failed", 0, TRUE);
		return;
	}

	// First call to EnumServicesStatusEx identifies the needed buffer size
	EnumServicesStatusEx(
		scHandle,
		SC_ENUM_PROCESS_INFO,
		SERVICE_WIN32,
		SERVICE_STATE_ALL,
		NULL,
		0,
		&BytesNeeded,
		&svcRetCount,
		resumeHandle,
		NULL);

	ENUM_SERVICE_STATUS_PROCESSW *buffer = (ENUM_SERVICE_STATUS_PROCESSW*)calloc(1, BytesNeeded);
	if (!buffer)
	{
		PrintMsg(NULL, "Memory Error\n");
		return;
	}

	DWORD buffSize = BytesNeeded;

	EnumServicesStatusEx(
		scHandle,
		SC_ENUM_PROCESS_INFO,
		SERVICE_WIN32,
		SERVICE_STATE_ALL,
		(LPBYTE)buffer,
		buffSize,
		&BytesNeeded,
		&svcRetCount,
		resumeHandle,
		NULL);

	for (DWORD idx = 0; idx < svcRetCount; idx++)
	{
		sprintf(servicebuf, "%s\n", buffer[idx].lpServiceName);
		PrintMsg(NULL, servicebuf);
	}

	return;
}

VOID WspCreateService()
{
	SC_HANDLE schSCManager;
	SC_HANDLE schService;
	CHAR binaryPath[MAX_PATH] = { 0 };
	CHAR serviceName[MAX_PATH] = { 0 };
	CHAR displayName[MAX_PATH] = { 0 };

	ConsolePrompt("Full Path Binary : ", binaryPath, MAX_PATH, TRUE);
	ConsolePrompt("Service Name     : ", serviceName, MAX_PATH, TRUE);
	ConsolePrompt("Display Name     : ", displayName, MAX_PATH, TRUE);


	// Get a handle to the SCM database. 

	schSCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
	if (NULL == schSCManager)
	{
		ReportError("OpenSCManager failed", 0, TRUE);
		return;
	}

	// Create the service
	schService = CreateService(
		schSCManager,              // SCM database 
		serviceName,               // name of service 
		displayName,               // service name to display 
		SERVICE_ALL_ACCESS,        // desired access 
		SERVICE_WIN32_OWN_PROCESS, // service type 
		SERVICE_DEMAND_START,      // start type 
		SERVICE_ERROR_NORMAL,      // error control type 
		binaryPath,                // path to service's binary 
		NULL,                      // no load ordering group 
		NULL,                      // no tag identifier 
		NULL,                      // no dependencies 
		NULL,                      // LocalSystem account 
		NULL);                     // no password 

	if (schService == NULL)
	{
		ReportError("CreateService failed", 0, TRUE);
		CloseServiceHandle(schSCManager);
		return;
	}
	PrintMsg(NULL, "Service installed successfully\n");

	CloseServiceHandle(schService);
	CloseServiceHandle(schSCManager);
}

VOID WspStartService()
{
	SERVICE_STATUS_PROCESS ssStatus;
	DWORD dwOldCheckPoint;
	DWORD dwStartTickCount;
	DWORD dwWaitTime;
	DWORD dwBytesNeeded;
	SC_HANDLE schSCManager;
	SC_HANDLE schService;
	CHAR serviceName[MAX_PATH] = { 0 };

	ConsolePrompt("Service Name     : ", serviceName, MAX_PATH, TRUE);

	// Get a handle to the SCM database. 
	schSCManager = OpenSCManager(
		NULL,                    // local computer
		NULL,                    // servicesActive database 
		SC_MANAGER_ALL_ACCESS);  // full access rights 

	if (NULL == schSCManager)
	{
		ReportError("OpenSCManager failed", 0, TRUE);
		return;
	}

	// Get a handle to the service.

	schService = OpenService(
		schSCManager,         // SCM database 
		serviceName,            // name of service 
		SERVICE_ALL_ACCESS);  // full access 

	if (schService == NULL)
	{
		ReportError("OpenService failed", 0, TRUE);
		CloseServiceHandle(schSCManager);
		return;
	}

	// Check the status in case the service is not stopped. 

	if (!QueryServiceStatusEx(
		schService,                     // handle to service 
		SC_STATUS_PROCESS_INFO,         // information level
		(LPBYTE)&ssStatus,             // address of structure
		sizeof(SERVICE_STATUS_PROCESS), // size of structure
		&dwBytesNeeded))              // size needed if buffer is too small
	{
		ReportError("QueryServiceStatusEx failed", 0, TRUE);
		CloseServiceHandle(schService);
		CloseServiceHandle(schSCManager);
		return;
	}

	// Check if the service is already running. It would be possible 
	// to stop the service here, but for simplicity this example just returns. 
	if (ssStatus.dwCurrentState != SERVICE_STOPPED && ssStatus.dwCurrentState != SERVICE_STOP_PENDING)
	{
		PrintMsg(NULL, "Cannot start the service because it is already running\n");
		CloseServiceHandle(schService);
		CloseServiceHandle(schSCManager);
		return;
	}

	// Save the tick count and initial checkpoint.
	dwStartTickCount = GetTickCount();
	dwOldCheckPoint = ssStatus.dwCheckPoint;

	// Wait for the service to stop before attempting to start it.
	while (ssStatus.dwCurrentState == SERVICE_STOP_PENDING)
	{
		PrintMsg(NULL, "Waiting For Service To Stop\n");
		// Do not wait longer than the wait hint. A good interval is 
		// one-tenth of the wait hint but not less than 1 second  
		// and not more than 10 seconds. 

		dwWaitTime = ssStatus.dwWaitHint / 10;

		if (dwWaitTime < 1000)
			dwWaitTime = 1000;
		else if (dwWaitTime > 10000)
			dwWaitTime = 10000;

		Sleep(dwWaitTime);

		// Check the status until the service is no longer stop pending. 
		if (!QueryServiceStatusEx(
			schService,                     // handle to service 
			SC_STATUS_PROCESS_INFO,         // information level
			(LPBYTE)&ssStatus,             // address of structure
			sizeof(SERVICE_STATUS_PROCESS), // size of structure
			&dwBytesNeeded))              // size needed if buffer is too small
		{
			ReportError("QueryServiceStatusEx failed", 0, TRUE);
			CloseServiceHandle(schService);
			CloseServiceHandle(schSCManager);
			return;
		}

		if (ssStatus.dwCheckPoint > dwOldCheckPoint)
		{
			// Continue to wait and check.
			dwStartTickCount = GetTickCount();
			dwOldCheckPoint = ssStatus.dwCheckPoint;
		}
		else
		{
			if (GetTickCount() - dwStartTickCount > ssStatus.dwWaitHint)
			{
				PrintMsg(NULL, "Timeout waiting for service to stop\n");
				CloseServiceHandle(schService);
				CloseServiceHandle(schSCManager);
				return;
			}
		}
	}

	// Attempt to start the service.

	if (!StartService(
		schService,  // handle to service 
		0,           // number of arguments 
		NULL))      // no arguments 
	{
		ReportError("StartService failed", 0, TRUE);
		CloseServiceHandle(schService);
		CloseServiceHandle(schSCManager);
		return;
	}
	else
		PrintMsg(NULL, "Starting Service\n");

	// Check the status until the service is no longer start pending. 

	if (!QueryServiceStatusEx(
		schService,                     // handle to service 
		SC_STATUS_PROCESS_INFO,         // info level
		(LPBYTE)&ssStatus,             // address of structure
		sizeof(SERVICE_STATUS_PROCESS), // size of structure
		&dwBytesNeeded))              // if buffer too small
	{
		ReportError("QueryServiceStatusEx failed", 0, TRUE);
		CloseServiceHandle(schService);
		CloseServiceHandle(schSCManager);
		return;
	}

	// Save the tick count and initial checkpoint.
	dwStartTickCount = GetTickCount();
	dwOldCheckPoint = ssStatus.dwCheckPoint;

	while (ssStatus.dwCurrentState == SERVICE_START_PENDING)
	{
		PrintMsg(NULL, ".");
		// Do not wait longer than the wait hint. A good interval is 
		// one-tenth the wait hint, but no less than 1 second and no 
		// more than 10 seconds. 

		dwWaitTime = ssStatus.dwWaitHint / 10;

		if (dwWaitTime < 1000)
			dwWaitTime = 1000;
		else if (dwWaitTime > 10000)
			dwWaitTime = 10000;

		Sleep(dwWaitTime);

		// Check the status again. 

		if (!QueryServiceStatusEx(
			schService,             // handle to service 
			SC_STATUS_PROCESS_INFO, // info level
			(LPBYTE)&ssStatus,             // address of structure
			sizeof(SERVICE_STATUS_PROCESS), // size of structure
			&dwBytesNeeded))              // if buffer too small
		{
			ReportError("QueryServiceStatusEx failed", 0, TRUE);
			break;
		}

		if (ssStatus.dwCheckPoint > dwOldCheckPoint)
		{
			// Continue to wait and check.
			dwStartTickCount = GetTickCount();
			dwOldCheckPoint = ssStatus.dwCheckPoint;
		}
		else
		{
			if (GetTickCount() - dwStartTickCount > ssStatus.dwWaitHint)
			{
				// No progress made within the wait hint.
				break;
			}
		}
	}

	// Determine whether the service is running.
	PrintMsg(NULL, "\n");

	if (ssStatus.dwCurrentState == SERVICE_RUNNING)
	{
		PrintMsg(NULL, "Service started successfully.\n");
	}
	else
	{
		PrintMsg(NULL, "Service not started\n");
	}

	CloseServiceHandle(schService);
	CloseServiceHandle(schSCManager);
}

VOID WspStopService()
{
	SERVICE_STATUS_PROCESS ssp;
	DWORD dwStartTime = GetTickCount();
	DWORD dwBytesNeeded;
	DWORD dwTimeout = 30000; // 30-second time-out
	DWORD dwWaitTime;
	SC_HANDLE schSCManager;
	SC_HANDLE schService;
	CHAR serviceName[MAX_PATH] = { 0 };

	// Get a handle to the SCM database.

	ConsolePrompt("Service Name     : ", serviceName, MAX_PATH, TRUE);

	schSCManager = OpenSCManager(
		NULL,                    // local computer
		NULL,                    // ServicesActive database 
		SC_MANAGER_ALL_ACCESS);  // full access rights 

	if (NULL == schSCManager)
	{
		ReportError("OpenSCManager failed", 0, TRUE);
		return;
	}

	// Get a handle to the service.

	schService = OpenService(
		schSCManager,         // SCM database 
		serviceName,            // name of service 
		SERVICE_STOP |
		SERVICE_QUERY_STATUS |
		SERVICE_ENUMERATE_DEPENDENTS);

	if (schService == NULL)
	{
		ReportError("OpenService failed", 0, TRUE);
		CloseServiceHandle(schSCManager);
		return;
	}

	// Make sure the service is not already stopped.

	if (!QueryServiceStatusEx(
		schService,
		SC_STATUS_PROCESS_INFO,
		(LPBYTE)&ssp,
		sizeof(SERVICE_STATUS_PROCESS),
		&dwBytesNeeded))
	{
		ReportError("QueryServiceStatusEx failed", 0, TRUE);
		goto stop_cleanup;
	}

	if (ssp.dwCurrentState == SERVICE_STOPPED)
	{
		PrintMsg(NULL, "Service is already stopped.\n");
		goto stop_cleanup;
	}

	// If a stop is pending, wait for it.

	while (ssp.dwCurrentState == SERVICE_STOP_PENDING)
	{
		PrintMsg(NULL, "Service stop pending...\n");

		// Do not wait longer than the wait hint. A good interval is 
		// one-tenth of the wait hint but not less than 1 second  
		// and not more than 10 seconds. 

		dwWaitTime = ssp.dwWaitHint / 10;

		if (dwWaitTime < 1000)
			dwWaitTime = 1000;
		else if (dwWaitTime > 10000)
			dwWaitTime = 10000;

		Sleep(dwWaitTime);

		if (!QueryServiceStatusEx(
			schService,
			SC_STATUS_PROCESS_INFO,
			(LPBYTE)&ssp,
			sizeof(SERVICE_STATUS_PROCESS),
			&dwBytesNeeded))
		{
			ReportError("QueryServiceStatusEx failed", 0, TRUE);
			goto stop_cleanup;
		}

		if (ssp.dwCurrentState == SERVICE_STOPPED)
		{
			PrintMsg(NULL, "Service stopped successfully.\n");
			goto stop_cleanup;
		}

		if (GetTickCount() - dwStartTime > dwTimeout)
		{
			PrintMsg(NULL, "Service stop timed out.\n");
			goto stop_cleanup;
		}
	}

	// Send a stop code to the service.
	if (!ControlService(
		schService,
		SERVICE_CONTROL_STOP,
		(LPSERVICE_STATUS)&ssp))
	{
		ReportError("ControlService failed", 0, TRUE);
		goto stop_cleanup;
	}

	// Wait for the service to stop.

	while (ssp.dwCurrentState != SERVICE_STOPPED)
	{
		Sleep(ssp.dwWaitHint);
		if (!QueryServiceStatusEx(
			schService,
			SC_STATUS_PROCESS_INFO,
			(LPBYTE)&ssp,
			sizeof(SERVICE_STATUS_PROCESS),
			&dwBytesNeeded))
		{
			ReportError("QueryServiceStatusEx failed", 0, TRUE);
			goto stop_cleanup;
		}

		if (ssp.dwCurrentState == SERVICE_STOPPED)
			break;

		if (GetTickCount() - dwStartTime > dwTimeout)
		{
			PrintMsg(NULL, "Wait timed out\n");
			goto stop_cleanup;
		}
	}
	PrintMsg(NULL, "Service stopped successfully\n");

stop_cleanup:
	CloseServiceHandle(schService);
	CloseServiceHandle(schSCManager);

}

VOID WspDeleteService()
{
	SC_HANDLE schSCManager;
	SC_HANDLE schService;
	SERVICE_STATUS ssStatus;
	CHAR serviceName[MAX_PATH] = { 0 };

	// Get a handle to the SCM database.

	ConsolePrompt("Service Name     : ", serviceName, MAX_PATH, TRUE);

	schSCManager = OpenSCManager(
		NULL,                    // local computer
		NULL,                    // ServicesActive database 
		SC_MANAGER_ALL_ACCESS);  // full access rights 

	if (NULL == schSCManager)
	{
		ReportError("OpenSCManager failed", 0, TRUE);
		return;
	}

	// Get a handle to the service.

	schService = OpenService(
		schSCManager,       // SCM database 
		serviceName,          // name of service 
		DELETE);            // need delete access 

	if (schService == NULL)
	{
		ReportError("OpenService failed", 0, TRUE);
		CloseServiceHandle(schSCManager);
		return;
	}

	// Delete the service.

	if (!DeleteService(schService))
	{
		printf("DeleteService failed (%d)\n", GetLastError());
	}
	else PrintMsg(NULL, "Service deleted successfully\n");

	CloseServiceHandle(schService);
	CloseServiceHandle(schSCManager);

}