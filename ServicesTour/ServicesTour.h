#include "..\common\common.h"

#define PRINT_SERVICE_LIST		1
#define CREATE_SERVICE			2
#define START_SERVICE			3
#define STOP_SERVICE			4
#define DELETE_SERVICE			5

VOID WspPrintServiceList();
VOID WspCreateService();
VOID WspStartService();
VOID WspStopService();
VOID WspDeleteService();