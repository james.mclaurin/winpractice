#ifndef _EXCEPT_TOUR
#define _EXCEPT_TOUR

#include <Windows.h>

#define EXCEPT_TOUR_DIVIDE  1
#define EXCEPT_TOUR_DEBUG   2
#define EXCEPT_TOUR_ACCESS  3
#define EXCEPT_TOUR_USER    4

BOOL ExceptTourAccess();
BOOL ExceptTourDivide();
BOOL ExceptTourDebug();
BOOL ExceptTourUser();

#endif