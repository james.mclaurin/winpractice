#include "..\common\common.h"
#include "ExceptionTour.h"

#define FIX_ME 0

static DWORD Filter(LPEXCEPTION_POINTERS pExceptionPointers, LPDWORD code);

int main(int argc, CHAR* argv[])
{
    DWORD   choice              = INVALID_MENU_ENTRY;

    do
    {
        choice = ConsolePromptMenu( TRUE,                      // Clear Screen
                                    "Choose your Exception\n",   // Title
                                    "Divide By Zero",          // Option 2
                                    "Debug Break",             // Option 3
                                    "Memory Access",
                                    "User Generated",          // Option 4
                                    NULL);                     // Must be NULL
        switch(choice)
        {
            // Finished
            case 0:
            break;

            // Options

            case EXCEPT_TOUR_DIVIDE:
               ExceptTourDivide();
            break;

            case EXCEPT_TOUR_DEBUG:
                ExceptTourDebug();
            break;

            case EXCEPT_TOUR_ACCESS:
                ExceptTourAccess();
            break;

            case EXCEPT_TOUR_USER:
                ExceptTourUser();
            break;

            // Unknown
            case INVALID_MENU_ENTRY:
            default:
                ConsolePromptPressEnter("Invalid Choice");
            break;

        }
    }while(choice != 0);

    return 0;
}

/* TODO:
    use __try / __except to catch EXCEPTION_INT_DIVIDE_BY_ZERO
    if correct exception is caught execute the handler if not
    continue searching exceptions

    ternary operators are helpful here:
    http://www.cprogramming.com/reference/operators/ternary-operator.html
*/
BOOL ExceptTourDivide()
{
    DWORD zero = 0;
    DWORD x;

    printf("EXCEPT_TOUR_DIVIDE\n\n");
    // TRY
	__try
    {
        x = 7/zero;
    }
    // EXCEPT ( if exception code equals EXCEPTION_INT_DIVIDE_BY_ZERO run our handler, otherwise continue search, ternerys will help here...)
	__except (GetExceptionCode() == EXCEPTION_INT_DIVIDE_BY_ZERO ? EXCEPTION_EXECUTE_HANDLER : EXCEPTION_CONTINUE_SEARCH)
    {
        printf("EXECUTION_HANDLER\n");
        ConsolePromptPressEnter("  Divided by zero\n\n");
        return FALSE;
    }
    
    return TRUE;
}

/* Catch DebugBreak() */
BOOL ExceptTourDebug()
{
    printf("EXCEPT_TOUR_DEBUG\n\n");
    //TRY
	__try
    {
        DebugBreak();
    }
    // EXCEPT ( if exception code equals EXCEPTIN_BREAKPOINT...) 
	__except (GetExceptionCode() == EXCEPTION_BREAKPOINT ? EXCEPTION_EXECUTE_HANDLER : EXCEPTION_CONTINUE_SEARCH)
    {
        printf("EXECUTION_HANDLER\n");
        ConsolePromptPressEnter("  Debugger not present\n\n");
        return FALSE;
    }
    ConsolePromptPressEnter("  Debugger present\n\n");
    
    return TRUE;
}

/* Complete using Filter Function with GetExceptionInformation */
BOOL ExceptTourAccess()
{
    DWORD code;
    LPDWORD ptrNull = NULL;

    printf("EXCEPT_TOUR_ACCESS\n");
    //TRY
	__try
    {
        *ptrNull = 73;
    }
    // EXCEPT handle in Filter Function
	__except (Filter(GetExceptionInformation(), &code))
    {
        ConsolePromptPressEnter("EXCEPTION HANDLER\n");
        return FALSE;
    }
    ConsolePromptPressEnter(NULL);
    return TRUE;
}

/* Complete using Filter Function with GetExceptionInformation */
BOOL ExceptTourUser()
{
    DWORD code;
    printf("EXCEPT_TOUR_USER\n");
    // TRY
	__try
    {
        ReportException("  Raising User Exception\n", 10);
    }
    // EXCEPT handle in Filter Function
	__except (Filter(GetExceptionInformation(), &code))
    {
        ConsolePromptPressEnter("EXCEPTION HANDLER\n");
        return FALSE;
    }
    ConsolePromptPressEnter(NULL);
    return TRUE;
}

DWORD Filter(LPEXCEPTION_POINTERS pExceptionPointers, LPDWORD code)
{
    *code = pExceptionPointers->ExceptionRecord->ExceptionCode;

    printf("FILTER\n");
    printf("  Code: 0x%08X\n", *code);

    // Handle User Exception
    // Why is this a user exception?
    if( (0x20000000 & *code) != 0)
    {
        printf("  Description: User Exception\n\n");
        return EXCEPTION_EXECUTE_HANDLER;
    }

    // Handle EXCEPTION_ACCESS_VIOLATION
    else if(*code == EXCEPTION_ACCESS_VIOLATION)
    {
        DWORD_PTR readWrite;
        DWORD_PTR virtAddr;

        // get the what type of access violation this is and store in readWrite
		readWrite = (DWORD)(pExceptionPointers->ExceptionRecord->ExceptionInformation[0]);

        // get the address the violation occured at
		virtAddr =  (DWORD)(pExceptionPointers->ExceptionRecord->ExceptionInformation[1]);

		printf("  Description: ");
        switch(readWrite)
        {
            case 0: 
                printf("Read Violation at address: 0x%08X\n\n", virtAddr);
            break;

            case 1:
                printf("Write Violation at address: 0x%08X\n\n", virtAddr);
            break;

            case 8:
                printf("Execute Violation at address: 0x%08X\n\n", virtAddr);
            break;
        }
        // return so that your handler will execute
		//return FIX_ME;
		return EXCEPTION_EXECUTE_HANDLER;
    }

    // return so that the search will continue for a handler
    //return FIX_ME;
    return EXCEPTION_CONTINUE_SEARCH;
}