#include "..\common\common.h"


int main(int argc, CHAR* argv[])
{
	HANDLE	hFileMap		= NULL;
	LPTSTR	pMemBase		= NULL;

	__try
	{	
		// Open a named file map: SHARED_MEMORY_NAME
		hFileMap = OpenFileMapping(FILE_MAP_ALL_ACCESS,
			FALSE,
			SHARED_MEMORY_NAME);

		if (hFileMap == INVALID_HANDLE_VALUE)
		{
			ReportError("Error mapping file\n", 0, TRUE);
			__leave;
		}

		// let process know we are now present, it can now close it's handle
		SignalProcessByEvent();

		// Map memory from opened named file map with size of SHARED_MEMORY_BUF_SIZE
		pMemBase = (LPTSTR)MapViewOfFile(hFileMap,
			FILE_MAP_ALL_ACCESS,
			0,
			0,
			SHARED_MEMORY_BUF_SIZE);

		if (pMemBase == NULL)
		{
			ReportError("Error Mapping view of file\n", 0, TRUE);
			__leave;
		}

		// print message stored in memory
		printf("Message Shared From Process: %s\n", pMemBase);
		ConsolePromptPressEnter(NULL);
	}
	__finally
	{
        /* cleanup */
		if (pMemBase != NULL)UnmapViewOfFile(pMemBase);
		if (hFileMap != NULL)CloseHandle(hFileMap);
	}

	return 0;
}



