#include "..\common\common.h"

int main(int argc, char *argv[])
{
    /* Create a process */


    PrintMsg(GetStdHandle(STD_OUTPUT_HANDLE), "Waiting for Process to end\n");
    
    /* Wait for spawned process to finish */

    PrintMsg(GetStdHandle(STD_OUTPUT_HANDLE), "Process Complete\n");
    return 0;
}