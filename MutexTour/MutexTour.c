#include "..\common\common.h"
#include "MutexTour.h"

#define M_CREATE_MUTEX			1
#define M_THREAD_WAIT_MUTEX		2
#define M_RELEASE_MUTEX			3
#define M_CLOSE_MUTEX			4

volatile DWORD  threads = 0;
volatile BOOL   bReleaseFlag = FALSE;
HANDLE          hMutex = NULL;
HANDLE          hThreadEvent = NULL;


int main(int argc, CHAR* argv[])
{
	DWORD   choice = 0;
	TCHAR	title[MAX_PATH] = { 0 };

	hThreadEvent = CreateEvent(NULL, FALSE, TRUE, NULL);
	do
	{
		sprintf(title, "Mutex Tour\n"
			"Mutex: %s\n"
			"Threads Waiting on mutex: %d\n"
			"\tBlack: Mutex not created\n"
			"\tGreen: Mutex Created and in released state\n"
			"\t Blue: Thread Waiting for released mutex\n",
			hMutex ? "Yes" : "No",
			threads);

		choice = ConsolePromptMenu(TRUE,
			/*T*/ title,
			/*1*/ "Create Mutex",
			/*2*/ "Thread Wait on Mutex",
			/*3*/ "Release Mutex",
			/*4*/ "Close Mutex",
			NULL);
		switch (choice)
		{
		case 0:

			break;

		case M_CREATE_MUTEX:
			if (hMutex != NULL)
			{
				ConsolePromptPressEnter("Mutex already Created\n");
				break;
			}
			/* TODO: Create an unamed owned Mutex with default attributes */

			ConsoleColor(BACKGROUND_GREEN);
			break;

		case M_THREAD_WAIT_MUTEX:
			if (hMutex == NULL)
			{
				ConsolePromptPressEnter("Mutex not created yet\n");
				break;
			}
			if (threads == 0)
			{
				ConsoleColor(BACKGROUND_BLUE);
			}
			CreateThread(NULL, 0, mThreadWaitMutex, NULL, 0, NULL);
			break;

		case M_RELEASE_MUTEX:
			if (hMutex == NULL)
			{
				ConsolePromptPressEnter("Mutex not created yet\n");
				break;
			}
			if (threads == 0)
			{
				ConsoleColor(BACKGROUND_GREEN);
			}
			ReleaseMutex(hMutex);
			bReleaseFlag = TRUE;
			break;

		case M_CLOSE_MUTEX:
			if (hMutex != NULL)
			{
				CloseHandle(hMutex);
				ConsoleColor(0);
				hMutex = NULL;
				threads = 0;
				break;
			}

			break;

		default:
			ConsolePromptPressEnter("\nInvalid Choice\n");
			break;
		}
		switch (WaitForSingleObject(hThreadEvent, 250))
		{
		case WAIT_OBJECT_0:

			break;
		}
	} while (choice != 0);
}

DWORD WINAPI mThreadWaitMutex(LPVOID lp)
{
	/* Increment global thread count: threads */
	InterlockedIncrement(&threads);

	/* Don't worry about this */
	SetEvent(hThreadEvent);

	/* TODO: Wait for mutex */

    /* TODO: Once returned Decrement threads waiting: threads
                           Decrement tokens, one has been retrieved */


	while (!bReleaseFlag)
	{
		Sleep(250);
	}

	/* TODO: reset flag to FALSE for next thread */

	return 0;
}
