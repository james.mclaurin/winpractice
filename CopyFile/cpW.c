#include <Windows.h>
#include <stdio.h>

#include "cpW.h"

#define BUF_SIZE 0x4000

int cpW(char *src, char *dst)
{
	HANDLE hIn, hOut;
	DWORD nIn, nOut;
	CHAR buffer [BUF_SIZE];

    if(!src || !dst)
    {
        printf("invalid file\n");
        return 1;
    }

	hIn = CreateFile(
            src, 
            GENERIC_READ, 
            FILE_SHARE_READ, 
            NULL,
			OPEN_EXISTING, 
            FILE_ATTRIBUTE_NORMAL, 
            NULL);

	if (hIn == INVALID_HANDLE_VALUE)
    {
		fprintf (stderr, "Cannot open input file. Error: %x\n", GetLastError ());
		return 2;
	}

	hOut = CreateFile(
        dst, 
        GENERIC_WRITE, 
        0, 
        NULL,
        CREATE_ALWAYS, 
        FILE_ATTRIBUTE_NORMAL, 
        NULL);

	if (hOut == INVALID_HANDLE_VALUE)
    {
		fprintf (stderr, "Cannot open output file. Error: %x\n", GetLastError ());
		CloseHandle(hIn);
		return 3;
	}

	while (ReadFile (hIn, buffer, BUF_SIZE, &nIn, NULL) && nIn > 0) 
    {
		WriteFile (hOut, buffer, nIn, &nOut, NULL);
		if (nIn != nOut) 
        {
			fprintf (stderr, "Fatal write error: %x\n", GetLastError ());
			CloseHandle(hIn); CloseHandle(hOut);
			return 4;
		}
	}

	CloseHandle (hIn);
	CloseHandle (hOut);
	return 0;
}
