#include <stdio.h>
#include <string.h>

#include "cpC.h"
#include "cpW.h"
#include "cpCF.h"

void usage()
{
    printf("\n  usage: CopyFile <cp | cpw | cpcf> <src> <dst>\n\n");
}

void main(int argc, char* argv[])
{
    int choice = 0;

	if(argc != 4)
    {
        usage();
        return;
    }
	
    if(!strnicmp(argv[1], "cp", 3))
        choice = 1;
    else if(!strnicmp(argv[1], "cpw", 4))
        choice = 2;
    else if(!strnicmp(argv[1], "cpcf", 5))
        choice = 3;
    else
    {
        usage();
        return;
    }

	switch(choice)
	{
		case 1:
			if(cpC(argv[2], argv[3]))  return;
		break;

		case 2:
			if(cpW(argv[2], argv[3]))  return;
		break;

		case 3:
			if(cpCF(argv[2], argv[3])) return;
		break;

		case 0:
            usage();
			return;
		break;

		default:
			printf("Not a valid choice\n");
		break;
	}

	printf("Program Completed Successfully\n");
	
	return ;
}
