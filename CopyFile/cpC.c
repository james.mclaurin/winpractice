#include "cpC.h"
#include <stdio.h>

#define BUF_SIZE 256

int cpC(char* src, char* dst)
{
	FILE *inFile, *outFile;
	char rec[BUF_SIZE];
    size_t bytesIn, bytesOut;

    if(!src || !dst)
    {
        printf("invalid file\n");
        return 1;
    }

	inFile = fopen (src, "rb");
	if (inFile == NULL) {
		perror (src);
		return 2;
	}

	outFile = fopen(dst, "wb");
	if (outFile == NULL) {
		perror (dst);
		fclose(inFile);
		return 3;
	}

	/* Process the input file a record at a time. */
	while ((bytesIn = fread (rec, 1, BUF_SIZE, inFile)) > 0) {
		bytesOut = fwrite (rec, 1, bytesIn, outFile);
		if (bytesOut != bytesIn) {
			perror ("Fatal write error.");
			fclose(inFile); fclose(outFile);
			return 4;
		}
	}

	fclose (inFile);
	fclose (outFile);
	return 0;

}