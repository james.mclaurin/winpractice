#include <Windows.h>
#include <stdio.h>

#include "cpCF.h"

int cpCF(char *src, char *dst)
{
	if(!src || !dst)
    {
        printf("invalid file\n");
        return 1;
    }

	if (!CopyFile(src, dst, FALSE))
    {
		fprintf (stderr, "CopyFile Error: %x\n", GetLastError ());
		return 2;
	}

	return 0;
}