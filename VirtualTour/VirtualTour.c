#include "..\common\common.h"
#include "VirtualTour.h"

static DWORD g_pageSize = 0;
static DWORD g_poolSize = 0;
static VOID VirtualTourSetPoolSize();
static DWORD VirtualTourGetPoolSize();
static VOID VirtualTourSetPageSize();
static DWORD VirtualTourGetPageSize();


int main(int argc, CHAR* argv[])
{
    DWORD       choice      = INVALID_MENU_ENTRY;
    LPVOID      memoryPool  = NULL;
    LPVOID      memChunks[VIRTUAL_TOUR_MAX_MEMORY_CHUNKS+1] = {NULL};
    DWORD       i;

    VirtualTourSetPageSize();
    VirtualTourSetPoolSize();

    do
    {
        /* Display memory chunks and their associated Address */
        for(i=0; i<VIRTUAL_TOUR_MAX_MEMORY_CHUNKS; i++)
        {
            printf("[%d] 0x%08X\n", i+1, memChunks[i]);
        }
        printf("\n");

        /* Once a memoryPool has been created display the different
           memory regions
        */
        if(memoryPool)
        {
            DWORD poolSize = VirtualTourGetPoolSize();
            LPVOID memoryPoolEnd = (PBYTE)memoryPool + poolSize;
            VirtualTourPrintMemoryInfo( memoryPool, memoryPoolEnd);
        }
        choice = ConsolePromptMenu( FALSE,                                // Clear Screen
                                    "\nVirtual Tour Options\n",            // Title
                                    "Create memory pool",                // Option 1
                                    "Allocate memory from pool",         // Option 2
                                    "Free memory from pool",             // Option 3
                                    "Destroy memory pool",               // Option 4
                                    "Change memory chunk to Executable", // Option 5
                                    NULL);

        switch(choice)
        {
            case VIRTUAL_TOUR_CREATE_POOL:
                if(memoryPool)
                {
                    ConsolePromptPressEnter("Pool Already Created\n");
                    break;
                }
                memoryPool = VirtualTourCreatePool();
            break;

            case VIRTUAL_TOUR_ALLOC:
            {
                CHAR prompt[MAX_PATH] = {0};
                CHAR response[8] = {0};
                CHAR chunk = 0;
                if(!memoryPool)
                {
                    ConsolePromptPressEnter("No Memory Pool Created\n");
                    break;
                }
                sprintf(prompt,"Which chunk would you like? [1-%d]]: ",VIRTUAL_TOUR_MAX_MEMORY_CHUNKS);
                ConsolePrompt(prompt, response, ARRAYSIZE(response), TRUE);
                chunk = atoi(response);
                if(chunk > 0 && chunk <= VIRTUAL_TOUR_MAX_MEMORY_CHUNKS)
                {
                    memChunks[chunk-1] = VirtualTourAlloc(memoryPool);
                }
            }
            break;

            case VIRTUAL_TOUR_FREE:
            {
                CHAR prompt[MAX_PATH] = {0};
                CHAR response[8] = {0};
                CHAR chunk = 0;
                if(!memoryPool)
                {
                    ConsolePromptPressEnter("No Memory Pool Created\n");
                    break;
                }
                sprintf(prompt,"Which chunk would you like? [1-%d]]: ",VIRTUAL_TOUR_MAX_MEMORY_CHUNKS);
                ConsolePrompt(prompt, response, ARRAYSIZE(response), TRUE);
                chunk = atoi(response);
                if(chunk > 0 && chunk <= VIRTUAL_TOUR_MAX_MEMORY_CHUNKS)
                {
                    VirtualTourFree(memChunks[chunk-1]);
                    memChunks[chunk-1] = NULL;
                }
            }
            break;

            case VIRTUAL_TOUR_DESTROY_POOL:
            {
                VirtualTourDestroyPool(memoryPool);
                for(i=0; i<VIRTUAL_TOUR_MAX_MEMORY_CHUNKS; i++)
                {
                    memChunks[i] = NULL;
                }
                memoryPool = NULL;
            }
            break;

            case VIRTUAL_TOUR_CHANGE_PAGE_EXE:
            {
                CHAR prompt[MAX_PATH] = {0};
                CHAR response[8] = {0};
                CHAR chunk = 0;
                if(!memoryPool)
                {
                    ConsolePromptPressEnter("No Memory Pool Created\n");
                    break;
                }
                sprintf(prompt,"Which chunk would you like? [1-%d]]: ",VIRTUAL_TOUR_MAX_MEMORY_CHUNKS);
                ConsolePrompt(prompt, response, ARRAYSIZE(response), TRUE);
                chunk = atoi(response);
                if(chunk > 0 && chunk <= VIRTUAL_TOUR_MAX_MEMORY_CHUNKS)
                {
                    VirtualTourPageExe(memChunks[chunk-1]);
                }
            }
            break;

        }
        ClearScreen();
    }while(choice != 0);

    return 0;
}


/* 
    Set Pool Size to have two less pages than the max menu array
    This allows you to see what happens when you request memory
    but don't have any available
*/
VOID VirtualTourSetPoolSize()
{
    DWORD       pageSize = VirtualTourGetPageSize();
    g_poolSize = (pageSize * VIRTUAL_TOUR_MAX_MEMORY_CHUNKS) - (pageSize * 2);
}

DWORD VirtualTourGetPoolSize()
{
    return g_poolSize;
}

/* TODO: 
    Use GetSystemInfo to get the system's page size 
    set g_pageSize to the system's page size
*/
VOID VirtualTourSetPageSize()
{
	SYSTEM_INFO systemInfo = { 0 };
	GetSystemInfo(&systemInfo);

	g_pageSize = systemInfo.dwPageSize;
}

DWORD VirtualTourGetPageSize()
{
    return g_pageSize;
}

/* Use Virtual Query to determine memory information */
BOOL VirtualTourPrintMemoryInfo( LPVOID memoryPoolStart, LPVOID memoryPoolStop )
{
    MEMORY_BASIC_INFORMATION    mbi;
    LPVOID                      ptr = memoryPoolStart;
    CHAR                        num[8];

    for(ptr = memoryPoolStart; ptr < memoryPoolStop; ptr = (PBYTE)ptr+mbi.RegionSize)
    {
        /* use virtual query to get memory information */
		if (!VirtualQuery(ptr, &mbi, sizeof(mbi)))
		{
			ReportError("VirtualQuery Error", 0, TRUE);
			return FALSE;
		}
  
        printf("0x%08X : 0x%08X%", ptr, (PBYTE)ptr + mbi.RegionSize);
        printf("    %-12s",
            (mbi.State == MEM_FREE    ? "MEM_FREE"    :
			 mbi.State == MEM_COMMIT  ? "MEM_COMMIT"  :
			 mbi.State == MEM_RESERVE ? "MEM_RESERVE" :
			 itoa(mbi.State, num, 16)));

        printf(("    %-24s\n"),
				(mbi.Protect & PAGE_EXECUTE				? "PAGE_EXECUTE"  :
				 mbi.Protect & PAGE_EXECUTE_READ		? "PAGE_EXECUTE_READ" :
				 mbi.Protect & PAGE_EXECUTE_READWRITE   ? "PAGE_EXECUTE_READWRITE"   :
				 mbi.Protect & PAGE_EXECUTE_WRITECOPY   ? "PAGE_EXECUTE_WRITECOPY"  :
				 mbi.Protect & PAGE_NOACCESS			? "PAGE_NOACCESS" :
				 mbi.Protect & PAGE_READWRITE			? "PAGE_READWRITE"   :
				 mbi.Protect & PAGE_WRITECOPY			? "PAGE_WRITECOPY" :
				 mbi.Protect & PAGE_READONLY			? "PAGE_READONLY"   :
                 mbi.Protect == 0                       ? "" :
				 itoa(mbi.Protect, num, 16)));
    }
    return TRUE;
}

/* Use Virtual Alloc to Create an initial pool */
LPVOID VirtualTourCreatePool()
{
    LPVOID      memoryPool = NULL;
    DWORD       poolSize = VirtualTourGetPoolSize();

    /* Allocate memory or memoryPool */
	memoryPool = VirtualAlloc(memoryPool, poolSize, MEM_RESERVE, PAGE_READWRITE);
	if (memoryPool == NULL)
	{
		ReportError("Error Creating Pool\n", 0, TRUE);
	}
   
    /* return memoryPool */
    return memoryPool;
}

LPVOID VirtualTourAlloc(LPVOID pool)
{
    DWORD                       poolSize    = VirtualTourGetPoolSize();
    DWORD                       pageSize    = VirtualTourGetPageSize();
    LPVOID                      memEnd      = (PBYTE)pool + poolSize;
    LPVOID                      ptr         = NULL;
    LPVOID                      mem         = NULL;
    MEMORY_BASIC_INFORMATION    mbi         = {0};
    
    for(ptr = pool; ptr < memEnd; ptr = (PBYTE)ptr+mbi.RegionSize)
    {
        /* query each page of memory that ptr points to and get information*/
		if (!VirtualQuery(ptr, &mbi, sizeof(mbi)))
		{
			ReportError("VirtualQuery Error", 0, TRUE);
			return FALSE;
		}
        /* if that page of memory is reserved than commit a pageSize of it and return it */
		if (mbi.State == MEM_RESERVE)
		{
			mem = VirtualAlloc(ptr, pageSize, MEM_COMMIT, PAGE_READWRITE);
			if (mem == NULL)
			{
				ReportError("Error VirtualAlloc", 0, TRUE);
				ConsolePromptPressEnter(NULL);
				continue;
			}
			return mem;
		}
    }
    ConsolePromptPressEnter("Virtual Tour: Out of Memory");
    return NULL;
}

VOID VirtualTourFree(LPVOID chunk)
{
	if (chunk)
	{
		if (!VirtualFree(chunk, VirtualTourGetPageSize(), MEM_DECOMMIT))
		{
			ReportError("Error VirtualFree", 0, TRUE);
		}
	}
}

VOID VirtualTourDestroyPool(LPVOID pool)
{
    /* Free your pool */
	VirtualFree(pool, 0, MEM_RELEASE);
}

BOOL VirtualTourPageExe(LPVOID chunk)
{
    DWORD old;
	if (chunk == NULL)
	{
		ConsolePromptPressEnter("Invalid Memory\n");
		return FALSE;
	}

	if (!VirtualProtect(chunk, VirtualTourGetPageSize(), PAGE_EXECUTE_READWRITE, &old))
	{
		ReportError("VirtualProtect Error", 0, TRUE);
		return FALSE;
	}
    
    return TRUE;
}