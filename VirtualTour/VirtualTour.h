#ifndef _VIRTUAL_TOUR
#define _VIRTUAL_TOUR

#include <Windows.h>

#define VIRTUAL_TOUR_CREATE_POOL        1
#define VIRTUAL_TOUR_ALLOC              2
#define VIRTUAL_TOUR_FREE               3
#define VIRTUAL_TOUR_DESTROY_POOL       4
#define VIRTUAL_TOUR_CHANGE_PAGE_EXE    5

#define VIRTUAL_TOUR_MAX_MEMORY_CHUNKS  8

BOOL VirtualTourPrintMemoryInfo( LPVOID memoryPoolStart, LPVOID memoryPoolStop );
LPVOID VirtualTourCreatePool();
LPVOID VirtualTourAlloc(LPVOID pool);
VOID VirtualTourFree(LPVOID chunk);
VOID VirtualTourDestroyPool(LPVOID POOL);
BOOL VirtualTourPageExe(LPVOID chunk);

#endif