/*
    The Sandbox is for you to utilize during class to experiment with code examples.
    This is helpful when working on large projects and you would like to isolate a little bit
    of code for testing.

    A good practice is to create your samples outside of main so you can save them for later.

    This project does not assume anything for UNICODE, you must #define _UNICODE and UNICODE
    if you would like to use them. This is done in the project settings under character set
    the value assigned is "not set".

    With this set, you generally can assume that you will be running under the "A" APIs, but
    you should be explicit.

*/

#include <Windows.h>
#include <stdio.h>

/* An example of using CreateFile that will Always Create the File, for more information
   on what each parameter means look at CreateFile on MSDN
   
   Test to see if it completed, display a message for each case
   
   If successful, delete the file upon completion

   makes no assumption of character set, explicitly uses wide characters
*/
void SandCreateFile()
{
    WCHAR filename[] = { L"Sandy.txt" };
    HANDLE hFile = CreateFileW(
        filename,
        GENERIC_ALL,                        // "RWX"
        FILE_SHARE_READ | FILE_SHARE_WRITE, // Sharing is Caring
        NULL,                               // Default security descriptor
        CREATE_ALWAYS,                      // Always Create, overwrite an existing file
        FILE_ATTRIBUTE_NORMAL,              // Normal File Attributes
        NULL);                              // No template used

    if (hFile == INVALID_HANDLE_VALUE)
    {
        /* Uh ohhhh */
        wprintf(L"Error Creating File: %d\n", GetLastError());
        return;
    }
    else
    {
        /* Don't need this anymore */
        CloseHandle(hFile);

        /* Print Success */
        wprintf(L"File Opened Successfully\n");

        /* Notice Delete File doesn't need a handle, just a name */
        DeleteFileW(filename);
        return;
    }
    return;
}

void main()
{
    /* Run Sample functions from here */

	int a = 5;
    /* Playing around with CreateFile */
	memcpy(NULL, &a, sizeof(int));

}