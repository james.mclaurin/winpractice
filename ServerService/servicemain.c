#include <Windows.h>
#include <strsafe.h>

#define SVC_ERROR_SERVICE_CTRL_DISPATCHER 0x00000001
#define SVC_ERROR_REGISTER_SVC_CTRL_HANDLER 0x00000002

SERVICE_STATUS          gSvcStatus;
SERVICE_STATUS_HANDLE   gSvcStatusHandle;
HANDLE                  ghSvcStopEvent = NULL;

static LPSTR serviceName = "ServerService";

void WINAPI SvcMain(DWORD argc, CHAR* argv[]);
void WINAPI SvcCtrlHandler(DWORD dwControl);
VOID ReportSvcStatus(DWORD dwCurrentState, DWORD dwWin32ExitCode, DWORD dwWaitHint);
VOID SvcReportEvent(LPSTR userMessage, DWORD gle, DWORD eventCode);

int main(int argc, CHAR* argv[])
{
	SERVICE_TABLE_ENTRY DispatchTable[] =
	{
		{ "", SvcMain },
		{ NULL, NULL }
	};

	if (!StartServiceCtrlDispatcher(DispatchTable))
	{
		SvcReportEvent(
			"StartServiceCtrlDispatcher Failed",
			GetLastError(),
			SVC_ERROR_SERVICE_CTRL_DISPATCHER
		);
		return SVC_ERROR_SERVICE_CTRL_DISPATCHER;
	}
}

VOID WINAPI SvcMain(DWORD argc, CHAR* argv[])
{
	// Register the handler function for the service

	gSvcStatusHandle = RegisterServiceCtrlHandler(
		serviceName,
		SvcCtrlHandler);

	if (!gSvcStatusHandle)
	{
		SvcReportEvent("RegisterServiceCtrlHandler", GetLastError(), SVC_ERROR_REGISTER_SVC_CTRL_HANDLER);
		return;
	}

	// These SERVICE_STATUS members remain as set here
	gSvcStatus.dwServiceType = SERVICE_WIN32_OWN_PROCESS;
	//gSvcStatus.dwServiceType = SERVICE_INTERACTIVE_PROCESS;
	gSvcStatus.dwServiceSpecificExitCode = 0;

	// Report initial status to the SCM
	ReportSvcStatus(SERVICE_START_PENDING, NO_ERROR, 3000);

	// TO_DO: Declare and set any required variables.
	//   Be sure to periodically call ReportSvcStatus() with 
	//   SERVICE_START_PENDING. If initialization fails, call
	//   ReportSvcStatus with SERVICE_STOPPED.

	// Create an event. The control handler function, SvcCtrlHandler,
	// signals this event when it receives the stop control code.
	ghSvcStopEvent = CreateEvent(
		NULL,    // default security attributes
		TRUE,    // manual reset event
		FALSE,   // not signaled
		NULL);   // no name

	if (ghSvcStopEvent == NULL)
	{
		ReportSvcStatus(SERVICE_STOPPED, NO_ERROR, 3000);
		return;
	}

	//ReportSvcStatus(SERVICE_START_PENDING, NO_ERROR, 3000);
	//Sleep(5000);

	// Report running status when initialization is complete.
	ReportSvcStatus(SERVICE_RUNNING, NO_ERROR, 0);

	DWORD entry = 0, bWritten;
	char buf[100];
	// TO_DO: Perform work until service stops.
	while (WaitForSingleObject(ghSvcStopEvent, 5000) == WAIT_TIMEOUT)
	{
		entry++;
		sprintf(buf, "[0x%08x] Entry Logged\n", entry);
		HANDLE hLog = CreateFile("C:\\ServerService.log", GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
		if (hLog == INVALID_HANDLE_VALUE)
			continue;
		SetFilePointer(hLog, 0, 0, FILE_END);
		WriteFile(hLog, buf, strlen(buf), &bWritten, NULL);
		CloseHandle(hLog);
	}

	// Check whether to stop the service.


	ReportSvcStatus(SERVICE_STOPPED, NO_ERROR, 0);
	return;
}


VOID ReportSvcStatus(DWORD dwCurrentState,
	DWORD dwWin32ExitCode,
	DWORD dwWaitHint)
{
	static DWORD dwCheckPoint = 1;

	// Fill in the SERVICE_STATUS structure.
	gSvcStatus.dwCurrentState = dwCurrentState;
	gSvcStatus.dwWin32ExitCode = dwWin32ExitCode;
	gSvcStatus.dwWaitHint = dwWaitHint;

	if (dwCurrentState == SERVICE_START_PENDING)
		gSvcStatus.dwControlsAccepted = 0;
	else gSvcStatus.dwControlsAccepted = SERVICE_ACCEPT_STOP;

	if ((dwCurrentState == SERVICE_RUNNING) ||
		(dwCurrentState == SERVICE_STOPPED))
		gSvcStatus.dwCheckPoint = 0;
	else gSvcStatus.dwCheckPoint = dwCheckPoint++;

	// Report the status of the service to the SCM.
	SetServiceStatus(gSvcStatusHandle, &gSvcStatus);
}


VOID WINAPI SvcCtrlHandler(DWORD dwCtrl)
{
	// Handle the requested control code. 

	switch (dwCtrl)
	{
	case SERVICE_CONTROL_STOP:
		ReportSvcStatus(SERVICE_STOP_PENDING, NO_ERROR, 0);

		// Signal the service to stop.

		SetEvent(ghSvcStopEvent);
		ReportSvcStatus(gSvcStatus.dwCurrentState, NO_ERROR, 0);

		return;

	case SERVICE_CONTROL_INTERROGATE:
		break;

	default:
		break;
	}

}

VOID SvcReportEvent(LPSTR userMessage, DWORD gle, DWORD eventCode)
{
	HANDLE hEventSource;
	LPCSTR lpszStrings[2];
	CHAR Buffer[80];
	if (userMessage && lstrlen(userMessage) > 0)
		return;

	hEventSource = RegisterEventSource(NULL, serviceName);

	if (eventCode != 0 && hEventSource != NULL)
	{
		StringCchPrintf(Buffer, 80, "[%d] failed with %d", userMessage, gle);

		lpszStrings[0] = serviceName;
		lpszStrings[1] = Buffer;

		ReportEvent(hEventSource,        // event log handle
			EVENTLOG_ERROR_TYPE, // event type
			0,                   // event category
			(0x0FFFFFFF & eventCode) | 0xE0000000,           // event identifier
			NULL,                // no security identifier
			2,                   // size of lpszStrings array
			0,                   // no binary data
			lpszStrings,         // array of strings
			NULL);               // no binary data
	}

	if (hEventSource != NULL)
		DeregisterEventSource(hEventSource);

	return;
}