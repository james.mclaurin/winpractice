#include "..\common\common.h"
#include "ThreadTour.h"

HANDLE			hThread = NULL;
HANDLE          hThreadEvent = NULL;
TCHAR	title[MAX_PATH] = { 0 };

int main(int argc, CHAR* argv[])
{
	DWORD   choice = 0;

	hThreadEvent = CreateEvent(NULL, FALSE, TRUE, NULL);
	do
	{
		sprintf(title, "Thread Tour\n"
			"\tBlack: Thread not created\n"
			"\tGreen: Thread created\n"
			"\t Blue: Thread suspended\n");

		choice = ConsolePromptMenu(TRUE,
			/*T*/ title,
			/*1*/ "Create Thread",
			/*2*/ "Suspend Thread",
			/*3*/ "Resume Thread",
			/*4*/ "Terminate Thread",
			NULL);

		switch (choice)
		{
		case 0:
			break;

		case THREAD_TOUR_CREATE:
			if (hThread)
			{
				ConsolePromptPressEnter("Thread Already Created\n");
				break;
			}

			ConsoleColor(BACKGROUND_GREEN);

			/* TODO: Create new thread, starting defaultMethod() */

			break;

		case THREAD_TOUR_SUSPEND:
			if (!hThread)
			{
				ConsolePromptPressEnter("Thread not created yet\n");
				break;
			}
			ConsoleColor(BACKGROUND_BLUE);

			/* TODO: Suspend the created thread */
			break;

		case THREAD_TOUR_RESUME:
			if (!hThread)
			{
				ConsolePromptPressEnter("Thread not created yet\n");
				break;
			}
			ConsoleColor(BACKGROUND_GREEN);

			/* TODO: Resume the created thread */
			break;

		case THREAD_TOUR_TERMINATE:

			/* TODO: Close the existing Thread */

			hThread = NULL;
			ConsoleColor(0);
			break;

		default:
			ConsolePromptPressEnter("\nInvalid Choice\n");
			break;
		}
		switch (WaitForSingleObject(hThreadEvent, 250))
		{
		case WAIT_OBJECT_0:

			break;
		}
	} while (choice != 0);
}


DWORD WINAPI defaultMethod(LPVOID lp)
{
	MessageBox(NULL, "Select OK to Continue", "ThreadTour", MB_OK);

	// Reset the Console Prompt Menu
	ConsoleColor(0);
	hThread = NULL;

	ConsolePromptMenu(TRUE,
		/*T*/ title,
		/*1*/ "Create Thread",
		/*2*/ "Suspend Thread",
		/*3*/ "Resume Thread",
		/*4*/ "Terminate Thread",
		NULL);

	return 0;
}
