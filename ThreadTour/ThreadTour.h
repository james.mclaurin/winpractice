#include <Windows.h>

#define THREAD_TOUR_CREATE		1
#define THREAD_TOUR_SUSPEND		2
#define THREAD_TOUR_RESUME		3
#define THREAD_TOUR_TERMINATE	4

DWORD WINAPI defaultMethod(LPVOID lp);