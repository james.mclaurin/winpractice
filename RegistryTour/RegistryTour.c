// RegistryTour.cpp : Defines the entry point for the console application.
//

#include "../common/common.h"

#define CHOICE_BUF                   4
#define WSP_REG_HKCU                 "HKEY_CURRENT_USER\\"
#define WSP_REG_NAME                 "Software\\WinSysProLab"
#define WSP_REG_NAME_TERM_SLASH      WSP_REG_NAME"\\"

VOID CreateKey();
VOID CheckForKey();
VOID DeleteKey();

VOID CreateDwordValue();
VOID CheckDwordValue();

VOID CreateStringValue();
VOID CheckStringValue();

VOID DeleteValue();

int main(int argc, CHAR* argv[])
{
    DWORD    ret;
    DWORD    choice        = 0;
    HKEY    hKeyWsp        = NULL;
    DWORD    disp;
    BOOL    finished    = FALSE;

    ret = RegCreateKeyEx( HKEY_CURRENT_USER,
                          WSP_REG_NAME,
                          0, NULL, 0, KEY_WRITE, NULL, &hKeyWsp, &disp);
    if(ret != ERROR_SUCCESS)
    {
        SetLastError(ret);
        ReportError("RegCreateKey Error", 0, TRUE);
        return 1;
    }
    RegCloseKey(hKeyWsp);

    do
    {
        choice = ConsolePromptMenu( TRUE, 
                                    "Registry Function Tour\n",    //Title 
                                    "Keys",                        //1
                                    "DWORD Values",                //2
                                    "String Values",            //3
                                    NULL);
        switch(choice)
        {
            // Keys
            case 1:
                choice = ConsolePromptMenu( TRUE,
                                            "Key Manipulation Menu\n",
                                            "Create Key",
                                            "Check For Key",
                                            "Delete Key",
                                            NULL);
                switch(choice)
                {
                    case 1: 
                        CreateKey(); 
                    break;

                    case 2: 
                        CheckForKey(); 
                    break;

                    case 3: 
                        DeleteKey(); 
                    break;

                    case INVALID_MENU_ENTRY:
                        ConsolePromptPressEnter("Not a valid choice");
                    break;
                }
            break;

            // DWORD Values
            case 2:
                choice = ConsolePromptMenu( TRUE,
                                            "DWORD Menu\n",
                                            "Create DWORD Value",
                                            "Check DWORD Value",
                                            "Delete DWORD Value",
                                            NULL);
                switch(choice)
                {
                    case 1: 
                        CreateDwordValue(); 
                    break;

                    case 2: 
                        CheckDwordValue();
                    break;

                    case 3: 
                        DeleteValue(); 
                    break;

                    case INVALID_MENU_ENTRY: 
                        ConsolePromptPressEnter("Not a valid choice");
                    break;
                }
            break;

            // String
            case 3:
                choice = ConsolePromptMenu( TRUE,
                                            "String Menu\n",
                                             "Create String Value",
                                             "Check String Value",
                                             "Delete String Value",
                                             NULL);
                switch(choice)
                {
                    case 1: 
                        CreateStringValue();
                    break;

                    case 2: 
                        CheckStringValue();
                    break;

                    case 3: 
                        DeleteValue(); 
                    break;

                    case INVALID_MENU_ENTRY:
                        ConsolePromptPressEnter("Not a valid choice");
                    break;
                }

            break;

            case 0:
                finished = TRUE;
            break;

            case INVALID_MENU_ENTRY:
                ConsolePromptPressEnter("Not a valid choice");
            break;
        }
    }while(!finished);

    RegDeleteTree(HKEY_CURRENT_USER, WSP_REG_NAME);

    return 0;
}

/* TODO:
    Open reg path WSP_REG_NAME
    Create Key
*/
VOID CreateKey()
{
    HKEY    hKeyRoot            = NULL;
    HKEY    hKeyNew                = NULL;
    CHAR    response[MAX_NAME]    = {0};
    DWORD   disp;
    DWORD   ret;
    BOOL    success                = FALSE;

    __try
    {
        if(!ConsolePrompt(
                ("Enter the new key name to create:\n  " WSP_REG_HKCU WSP_REG_NAME_TERM_SLASH), 
                response, 
                MAX_NAME, 
                TRUE))
            __leave;

        /* Open Registry Key WSP_REG_NAME */
		if (ERROR_SUCCESS != RegOpenKeyEx(
			HKEY_CURRENT_USER,
			WSP_REG_NAME,
			0,
			KEY_CREATE_SUB_KEY,
			&hKeyRoot))
		{
			ReportError("Error opening Registry\n", 0, TRUE);
			success = FALSE;
			__leave;
		}

        /* Create Registry Key */
		if (ERROR_SUCCESS == RegCreateKeyEx(
			hKeyRoot,
			response,
			0, NULL,
			0, KEY_WRITE,
			NULL,
			&hKeyNew,
			&disp))
		{
			ReportError("Error opening Registry\n", 0, TRUE);
			success = FALSE;
			__leave;
		}
        
        success = TRUE;
    }
    __finally
    {
        CHAR msg[MAX_PATH] = {0};

        /*TODO: cleanup */
		if (hKeyRoot != INVALID_HANDLE_VALUE)
			RegCloseKey(hKeyRoot);
		if (hKeyNew != INVALID_HANDLE_VALUE)
		RegCloseKey(hKeyNew);
        
        sprintf_s(msg, MAX_PATH, "CreateKey: %s\n", success?"SUCCESS":"FAILED");
        ConsolePromptPressEnter(msg);
    }
    
    return;
}

/* TODO:
    Check for key
*/
VOID CheckForKey()
{
    HKEY    hKeyRoot            = NULL;
    DWORD    ret;
    CHAR    response[MAX_NAME]  = {0};
    CHAR    fullKey[MAX_NAME]    = {0};

    
    if(!ConsolePrompt(("Enter the keyname to check for: \n  "WSP_REG_HKCU WSP_REG_NAME_TERM_SLASH), 
                    response, MAX_NAME, TRUE))
    {
        return;
    }
    sprintf_s(fullKey, MAX_NAME, "%s%s", WSP_REG_NAME_TERM_SLASH, response);

    /* Check for key */
	if (ERROR_SUCCESS != RegOpenKeyEx(HKEY_CURRENT_USER,
		fullKey,
		0, KEY_CREATE_SUB_KEY, &hKeyRoot))
	{
		ReportError("Error finding key\n", 0, TRUE);
		return;
	}
    

    /* Cleanup */
    if(hKeyRoot != NULL)RegCloseKey(hKeyRoot);

    return;
}

/* TODO:
    Delete Key
*/
VOID DeleteKey()
{
    DWORD    ret;
    CHAR    response[MAX_NAME]  = {0};
    CHAR    fullKey[MAX_NAME]    = {0};

    if(!ConsolePrompt( ("Enter the keyname to delete: "WSP_REG_HKCU WSP_REG_NAME_TERM_SLASH), 
                    response, MAX_NAME, TRUE))
    {
        return;
    }

    sprintf_s(fullKey, MAX_NAME, "%s%s", WSP_REG_NAME_TERM_SLASH, response);


    /* Delete Key */
	if (ERROR_SUCCESS != RegDeleteKey(HKEY_CURRENT_USER, fullKey))
	{
		ReportError("Error deleting key\n", 0, TRUE);
	}
    
    return;
}

/* TODO:
    Open Key
    Set Value
*/
VOID CreateDwordValue()
{
    HKEY    hKeyRoot;
    CHAR    key[MAX_NAME]        = {0};
    CHAR    value[MAX_NAME]        = {0};
    CHAR    data[MAX_NAME]        = {0};
    CHAR    response[MAX_NAME]  = {0};
    CHAR    keyvalue[MAX_NAME]  = {0};
    DWORD    dData                = 0;

    DWORD    ret;

    ConsolePrompt( ("Enter key name to use: "
                    WSP_REG_HKCU WSP_REG_NAME_TERM_SLASH), 
                    key, MAX_NAME, TRUE);

    ConsolePrompt( "Enter the Value Name: ", 
                    value, MAX_NAME, TRUE);

    ConsolePrompt( "Enter the DWORD data: ", 
                    data, MAX_NAME, TRUE);

    dData = atoi(data);
    sprintf_s( keyvalue, MAX_NAME, "%s%s", WSP_REG_NAME_TERM_SLASH, key  );

    __try
    {
		if (ERROR_SUCCESS != RegOpenKeyEx(HKEY_CURRENT_USER,
			keyvalue,
			0, KEY_WRITE, &hKeyRoot))
		{
			ReportError("Error deleting key\n", 0, TRUE);
			__leave;
		}
        /* Set Value:value to data:dData */
		if (ERROR_SUCCESS != RegSetValueEx(
			hKeyRoot,
			value,
			0,
			REG_DWORD,
			(const BYTE*)&dData,
			sizeof(DWORD)))
		{
			ReportError("Error setting value data\n", 0, TRUE);
			__leave;
		}

    }
    __finally
    {
        /* Cleanup */
		RegCloseKey(hKeyRoot);
    }

    return;
}

/* TODO:
    Open Key
    Query Value
*/
VOID CheckDwordValue()
{
    HKEY    hKeyRoot            = NULL;
    CHAR    key[MAX_NAME]        = {0};
    CHAR    value[MAX_NAME]        = {0};
    CHAR    prompt[MAX_NAME]    = {0};
    CHAR    response[MAX_NAME]  = {0};
    CHAR    keyvalue[MAX_NAME]  = {0};
    DWORD    ret;
    DWORD    type;
    DWORD    data                = 0;
    DWORD    cbData                = sizeof(DWORD);

    ConsolePrompt( ("Enter key name to use: " WSP_REG_HKCU WSP_REG_NAME_TERM_SLASH), 
                    key, MAX_NAME, TRUE);

    ConsolePrompt( "Enter the Value Name: ", 
                    value, MAX_NAME, TRUE);

    sprintf_s( keyvalue, MAX_NAME, "%s%s", WSP_REG_NAME_TERM_SLASH, key  );

    __try
    {
        /* Open key keyvalue */
		if (ERROR_SUCCESS != RegOpenKeyEx(HKEY_CURRENT_USER,
			keyvalue,
			0, KEY_WRITE, &hKeyRoot))
		{
			ReportError("Error deleting key\n", 0, TRUE);
			__leave;
		}
        
        /* Query value and print it out */
		if (ERROR_SUCCESS != RegQueryValueEx(
			hKeyRoot,
			value,
			0,
			&type,
			(LPBYTE)&data,
			&cbData))
		{
			ReportError("Error querying value\n", 0, TRUE);
			__leave;
		}
		PrintMsg("Query Result: %d\n", data);

    }
    __finally
    {
        /* cleanup */
		RegCloseKey(hKeyRoot);
    }

    
    return;
}

/* TODO:
    Open Key
    Set Value
*/
VOID CreateStringValue()
{
    HKEY    hKeyRoot            = NULL;
    CHAR    key[MAX_NAME]        = {0};
    CHAR    value[MAX_NAME]        = {0};
    CHAR    data[MAX_NAME]        = {0};
    CHAR    response[MAX_NAME]  = {0};
    CHAR    keyvalue[MAX_NAME]  = {0};

    DWORD    ret;

    ConsolePrompt( ("Enter key name to use: "
                    WSP_REG_HKCU WSP_REG_NAME_TERM_SLASH), 
                    key, MAX_NAME, TRUE);

    ConsolePrompt( "Enter the Value Name: ", 
                    value, MAX_NAME, TRUE);

    ConsolePrompt( "Enter the String data: ", 
                    data, MAX_NAME, TRUE);

    sprintf_s( keyvalue, MAX_NAME, "%s%s", WSP_REG_NAME_TERM_SLASH, key  );

    __try
    {
        /* Open Key keyvalue */
		if (ERROR_SUCCESS != RegOpenKeyEx(HKEY_CURRENT_USER,
			keyvalue,
			0, KEY_WRITE, &hKeyRoot))
		{
			ReportError("Error deleting key\n", 0, TRUE);
			__leave;
		}
        /* Set Value */
		if (ERROR_SUCCESS != RegSetValueEx(
			hKeyRoot,
			value,
			0,
			REG_DWORD,
			(const BYTE*)&value,
			sizeof(DWORD)))
		{
			ReportError("Error setting value data\n", 0, TRUE);
			__leave;
		}
    }
    __finally
    {
        /* Cleanup */
		RegCloseKey(hKeyRoot);
    }
    return;
}

/* TODO:
    Open key
    Query Value
*/
VOID CheckStringValue()
{
    HKEY    hKeyRoot            = NULL;
    CHAR    key[MAX_NAME]        = {0};
    CHAR    value[MAX_NAME]        = {0};
    CHAR    prompt[MAX_NAME]    = {0};
    CHAR    response[MAX_NAME]  = {0};
    CHAR    keyvalue[MAX_NAME]  = {0};
    DWORD    ret;
    DWORD    type;
    CHAR    data[MAX_NAME]        = {0};
    DWORD    cbData                = MAX_NAME;

    ConsolePrompt( ("Enter key name to use: " WSP_REG_HKCU WSP_REG_NAME_TERM_SLASH), 
                    key, MAX_NAME, TRUE);

    ConsolePrompt( "Enter the Value Name: ", 
                    value, MAX_NAME, TRUE);

    sprintf_s( keyvalue, MAX_NAME, "%s%s", WSP_REG_NAME_TERM_SLASH, key  );

    
    __try
    {
        /* open key:keyvalue */
		if (ERROR_SUCCESS != RegOpenKeyEx(HKEY_CURRENT_USER,
			keyvalue,
			0, KEY_WRITE, &hKeyRoot))
		{
			ReportError("Error deleting key\n", 0, TRUE);
			__leave;
		}

        /* Query value and print it out */
		if (ERROR_SUCCESS != RegQueryValueEx(
			hKeyRoot,
			value,
			0,
			&type,
			(LPBYTE)&data,
			&cbData))
		{
			ReportError("Error querying value\n", 0, TRUE);
			__leave;
		}
		PrintMsg("Query Result: %s\n", data);
    }
    __finally
    {
        /* cleanup */
		RegCloseKey(hKeyRoot);
    }
    
    return;
}

/* TODO:
  Open Key
  Delete Value
*/
VOID DeleteValue()
{
    HKEY    hKeyRoot            = NULL;
    DWORD    ret;
    CHAR    response[MAX_NAME]  = {0};
    CHAR    key[MAX_NAME]        = {0};
    CHAR    value[MAX_NAME]        = {0};
    CHAR    fullkey[MAX_NAME]    = {0};

    ConsolePrompt( ("Enter key name to use: "WSP_REG_HKCU WSP_REG_NAME_TERM_SLASH), 
                    key, MAX_NAME, TRUE);

    ConsolePrompt( "Enter the Value Name: ", 
                    value, MAX_NAME, TRUE);

    sprintf_s(fullkey, MAX_NAME, "%s%s", WSP_REG_NAME_TERM_SLASH, key);

    __try
    {

        /* Open Key */
		if (ERROR_SUCCESS != RegOpenKeyEx(HKEY_CURRENT_USER,
			fullkey,
			0, KEY_WRITE, &hKeyRoot))
		{
			ReportError("Error deleting key\n", 0, TRUE);
			__leave;
		}

        /* Delete Value */

        
    }
    __finally
    {
        /* Cleanup */
		RegCloseKey(hKeyRoot);
    }
    return;
}


