#include "..\common\common.h"
#include "MemoryMappedPE.h"

int main(int argc, CHAR* argv[])
{
    DWORD   choice              = 0;
    CHAR    filename[MAX_PATH]  = {0};
    LPVOID  memory              = NULL;
    DWORD   memorySize          = 0;

    
	do
	{
        printf("File Name  : %s\n", filename);
        printf("Memory Map : 0x%08X\n\n", memory);
		choice = ConsolePromptMenu( FALSE,
								    /*T*/ "MemoryMappedPE Menu:\n",
                                    /*1*/ "Set File Name",
									/*2*/ "Get Memory Map of PE",
									/*3*/ "DLL or EXE",
                                    /*4*/ "Hexdump",
                                    NULL);
		switch(choice)
		{
			case 0:
				
			break;

			case MEMORY_MAPPED_PE_SET_FILE_NAME:
				ConsolePrompt("Enter Filename: ", filename, MAX_PATH, TRUE);
                if(memory)
                {
                    UnmapViewOfFile(memory);
                    memory = NULL;
                    memorySize = 0;
                }
			break;

            case MEMORY_MAPPED_PE_GET_MAP:
                if(memory)
                {
                    UnmapViewOfFile(memory);
                    memory = NULL;
                }
                if(!strcmp(filename,""))
                    ConsolePromptPressEnter("No Filename Set\n");
                else
                    MemoryMappedPeGetMap(filename, &memory, &memorySize);
                ConsolePromptPressEnter(NULL);
            break;

            case MEMORY_MAPPED_PE_DLL_OR_EXE:
                if(!memory)
                    ConsolePromptPressEnter("No memory mapped\n");
                else
                {
                    MemoryMappedPeDllOrExe(memory);
                    ConsolePromptPressEnter(NULL);
                }
            break;

			case MEMORY_MAPPED_PE_HEXDUMP:
				if(!memory)
                    ConsolePromptPressEnter("No memory mapped\n");
                else
                {
                    HexDump(memory,memorySize);
                    ConsolePromptPressEnter(NULL);
                }
			break;

			default:
				printf("Invalid Choice");
			break;
		}
        ClearScreen();
	}while(choice != 0);

    /* TODO: clean up */
    if(memory)UnmapViewOfFile(memory);

	return 0;
}

/* TODO:
    Get memory pointer to mapped file
*/
BOOL MemoryMappedPeGetMap(LPSTR filename, LPVOID *memory, DWORD *memorySize)
{
    BOOL    success         = FALSE;
    HANDLE  hFile           = INVALID_HANDLE_VALUE;
    HANDLE  hFileMapping    = NULL;
    DWORD   filesize        = 0;
    LPVOID  pFileBase       = NULL;
    __try
    {
        /* create a file handle to filename */
		hFile = CreateFile(
			filename,
			GENERIC_READ,
			FILE_SHARE_READ,
			NULL,
			OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL,
			0);
		if (hFile == INVALID_HANDLE_VALUE)
		{
			ReportError("Error opening file\n", 0, TRUE);
			__leave;
		}

        /* get the file's filesize */
		filesize = GetFileSize(hFile, NULL);

        /* Create the file mapping */
		hFileMapping = CreateFileMapping(
			hFile,
			NULL,
			PAGE_READONLY,
			0,
			0,
			NULL);
		if (hFileMapping == INVALID_HANDLE_VALUE)
		{
			ReportError("Error mapping memory\n", 0, TRUE);
			__leave;
		}

        /* Map a view of the file */
		pFileBase = MapViewOfFile(hFileMapping, FILE_MAP_READ, 0, 0, 0);
        success = TRUE;
    }
    __finally
    {
        if(success)
        {
            /* return memorySize and memory from filesize and pFileBase */
			*memorySize = filesize;
			*memory = pFileBase;
        }
        /* Cleanup, make sure to only cleanup what is needed currently */
		if (hFile != INVALID_HANDLE_VALUE)CloseHandle(hFile);
		if (hFileMapping != NULL)CloseHandle(hFileMapping);
    }
    return success;
}

/* TODO:
    See how easy it is to move around the file instead of a bunch or read and write files
*/
BOOL MemoryMappedPeDllOrExe(LPVOID memory)
{
    PIMAGE_DOS_HEADER	pDosHeader		= NULL;
	PIMAGE_NT_HEADERS	pNtHeader		= NULL;

    pDosHeader = (PIMAGE_DOS_HEADER)memory;
	if(pDosHeader->e_magic != IMAGE_DOS_SIGNATURE)
	{
		printf("IMAGE_DOS_SIGNATURE FAIL : 0x%hhX\n", pDosHeader->e_magic );
		return FALSE;
	}
	printf("IMAGE_DOS_SIGNATURE : PASS\n");

	pNtHeader = (PIMAGE_NT_HEADERS)((PBYTE)pDosHeader + pDosHeader->e_lfanew);
	if(pNtHeader->Signature != IMAGE_NT_SIGNATURE)
	{
		printf("IMAGE_NT_SIGNATURE FAIL :  0x%hhX\n", pNtHeader->Signature );
		return FALSE;
	}
	printf("IMAGE_NT_SIGNATURE  : PASS\n");
		
	/* Check if file is executable... DLLs are executable */
	if((pNtHeader->FileHeader.Characteristics & IMAGE_FILE_EXECUTABLE_IMAGE) == IMAGE_FILE_EXECUTABLE_IMAGE)
	{
		printf("EXECUTABLE FILE     : TRUE\n");
	}

	/* Check if file is DLL */
	if((pNtHeader->FileHeader.Characteristics & IMAGE_FILE_DLL) == IMAGE_FILE_DLL)
	{
		printf("DLL FILE            : TRUE\n");
	}
    return TRUE;
}