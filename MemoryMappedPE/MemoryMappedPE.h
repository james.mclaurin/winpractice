#include "..\common\common.h"

#define MEMORY_MAPPED_PE_SET_FILE_NAME  1
#define MEMORY_MAPPED_PE_GET_MAP		2
#define MEMORY_MAPPED_PE_DLL_OR_EXE     3
#define MEMORY_MAPPED_PE_HEXDUMP        4

BOOL MemoryMappedPeGetMap(LPSTR filename, LPVOID *memory, DWORD *memorySize);
BOOL MemoryMappedPeDllOrExe(PVOID memMap);