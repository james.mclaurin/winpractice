#ifndef _HEAP_TOUR
#define _HEAP_TOUR

#include <Windows.h>

/* Understanding this section is not neccasary */
typedef void (WINAPI*ExecHeapType)(void);
#pragma runtime_checks("s", off)
static void WINAPI HeapFunction(){puts("Execution from the heap successful\n");}
static void marker(){}
#pragma runtime_checks("", restore)
#define HEAP_TOUR_MEMORY_SIZE ((DWORD)marker - (DWORD)HeapFunction)
/* End of non understanding section */

#define HEAP_TOUR_CREATE_EXEC_HEAP       1
#define HEAP_TOUR_CREATE_NON_EXEC_HEAP   2
#define HEAP_ALLOCATE_MEMORY             3
#define HEAP_PUT_TEXT_IN_MEMORY          4
#define HEAP_PUT_CODE_IN_MEMORY          5
#define HEAP_READ_MEMORY                 6
#define HEAP_EXECUTE_MEMORY              7
#define HEAP_FREE_MEMORY                 8
#define HEAP_DESTROY_HEAP                9

BOOL HeapTourCreateExecutableHeap();
BOOL HeapTourCreateNonExecutableHeap();
BOOL HeapTourAllocateMemory();
BOOL HeapTourPutTextInMemory();
BOOL HeapTourPutCodeInMemory();
BOOL HeapTourReadMemory();
BOOL HeapTourExecuteMemory();
BOOL HeapTourFreeMemory();
BOOL HeapTourDestroyHeap();

#endif
