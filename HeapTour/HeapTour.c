#include "..\common\common.h"
#include "HeapTour.h"

HANDLE  hUserHeap = NULL;
LPVOID  heapChunk = NULL;

static DWORD Filter(LPEXCEPTION_POINTERS pExP);

int main(int argc, CHAR* argv[])
{
	DWORD   choice = INVALID_MENU_ENTRY;

	do
	{
		choice = ConsolePromptMenu(TRUE,                            // Clear Screen
			"Heap Options\n",                // Title
			"Create Executable Heap",        // Option 1
			"Create Non-Executable Heap",    // Option 2
			"Allocate Memory",               // Option 3
			"Put text in memory",            // Option 4
			"Put Executable code in memory", // Option 5
			"Read memory",                   // Option 6
			"Execute memory",                // Option 7
			"Free Memory",                   // Option 8
			"Destroy Heap",                  // Option 9
			NULL);                           // Must be NULL
		switch (choice)
		{
			// Finished
		case 0:
			break;

			// Options
		case HEAP_TOUR_CREATE_EXEC_HEAP:
			HeapTourCreateExecutableHeap();
			break;

		case HEAP_TOUR_CREATE_NON_EXEC_HEAP:
			HeapTourCreateNonExecutableHeap();
			break;

		case HEAP_ALLOCATE_MEMORY:
			HeapTourAllocateMemory();
			break;

		case HEAP_PUT_TEXT_IN_MEMORY:
			HeapTourPutTextInMemory();
			break;

		case HEAP_PUT_CODE_IN_MEMORY:
			HeapTourPutCodeInMemory();
			break;

		case HEAP_READ_MEMORY:
			HeapTourReadMemory();
			break;

		case HEAP_EXECUTE_MEMORY:
			HeapTourExecuteMemory();
			break;

		case HEAP_FREE_MEMORY:
			HeapTourFreeMemory();
			break;

		case HEAP_DESTROY_HEAP:
			HeapTourDestroyHeap();
			break;

			// Unknown
		case INVALID_MENU_ENTRY:
		default:
			ConsolePromptPressEnter("Invalid Choice");
			break;

		}
	} while (choice != 0);

	return 0;
}

BOOL HeapTourCreateExecutableHeap()
{
	if (hUserHeap != NULL)
	{
		ConsolePromptPressEnter("Heap already created");
		return FALSE;
	}

	hUserHeap = HeapCreate(
		HEAP_CREATE_ENABLE_EXECUTE | HEAP_GENERATE_EXCEPTIONS,
		0,
		0);

	if (hUserHeap == NULL)
	{
		ReportError("HeapCreate Error", 0, TRUE);
		ConsolePromptPressEnter(NULL);
		return FALSE;
	}
	ConsolePromptPressEnter("SUCCESS");
	return TRUE;

}

BOOL HeapTourCreateNonExecutableHeap()
{
	if (hUserHeap != NULL)
	{
		ConsolePromptPressEnter("Heap already created");
		return FALSE;
	}

	hUserHeap = HeapCreate(
		HEAP_GENERATE_EXCEPTIONS,
		0,
		0);

	if (hUserHeap == NULL)
	{
		ReportError("HeapCreate Error", 0, TRUE);
		ConsolePromptPressEnter(NULL);
		return FALSE;
	}
	ConsolePromptPressEnter("SUCCESS");
	return TRUE;
}

/* Allocate memory in the size of HEAP_TOUR_MEMORY_SIZE */
BOOL HeapTourAllocateMemory()
{
	if (heapChunk != NULL)
	{
		ConsolePromptPressEnter("Heap memory already allocated");
		return FALSE;
	}

	__try
	{
		heapChunk = HeapAlloc(hUserHeap, HEAP_ZERO_MEMORY | HEAP_GENERATE_EXCEPTIONS, HEAP_TOUR_MEMORY_SIZE);
	}
	__except (Filter(GetExceptionInformation()))
	{
		ConsolePromptPressEnter(NULL);
		return FALSE;
	}

	ConsolePromptPressEnter("SUCCESS");
	return TRUE;
}

BOOL HeapTourPutTextInMemory()
{
	if (heapChunk == NULL)
	{
		ConsolePromptPressEnter("No Memory Allocated from Heap");
		return FALSE;
	}
	sprintf((CHAR*)heapChunk, "Hello text placed on the heap\n");
	ConsolePromptPressEnter("SUCCESS");
	return TRUE;
}

BOOL HeapTourPutCodeInMemory()
{
	if (heapChunk == NULL)
	{
		ConsolePromptPressEnter("No Memory Allocated from Heap");
		return FALSE;
	}
	memcpy(heapChunk, HeapFunction, HEAP_TOUR_MEMORY_SIZE);

	ConsolePromptPressEnter("SUCCESS");
	return TRUE;
}

BOOL HeapTourReadMemory()
{
	if (heapChunk == NULL)
	{
		ConsolePromptPressEnter("No Memory Allocated from Heap");
		return FALSE;
	}
	HexDump(heapChunk, HEAP_TOUR_MEMORY_SIZE);
	ConsolePromptPressEnter("SUCCESS");
	return TRUE;
}

BOOL HeapTourExecuteMemory()
{
	if (heapChunk == NULL)
	{
		ConsolePromptPressEnter("No Memory Allocated from Heap");
		return FALSE;
	}

	__try
	{
		((ExecHeapType)heapChunk)();
	}
	__except (Filter(GetExceptionInformation()))
	{
		ConsolePromptPressEnter(NULL);
		return FALSE;
	}
	ConsolePromptPressEnter("SUCCESS");
	return TRUE;
}

BOOL HeapTourFreeMemory()
{
	if (heapChunk == NULL)
	{
		ConsolePromptPressEnter("No Memory Allocated from Heap");
		return FALSE;
	}

	if (!HeapFree(hUserHeap, 0, heapChunk))
	{
		ReportError("Error HeapFree", 0, TRUE);
		return FALSE;
	}
	heapChunk = NULL;
	ConsolePromptPressEnter("SUCCESS");
	return TRUE;
}

BOOL HeapTourDestroyHeap()
{
	if (hUserHeap == NULL)
	{
		ConsolePromptPressEnter("No Heap Allocated");
		return FALSE;
	}

	if (!HeapDestroy(hUserHeap))
	{
		ReportError("Error HeapFree", 0, TRUE);
		return FALSE;
	}

	hUserHeap = NULL;
	heapChunk = NULL;
	ConsolePromptPressEnter("SUCCESS");
	return TRUE;
}

DWORD Filter(LPEXCEPTION_POINTERS pExceptionPointers)
{
	DWORD code = pExceptionPointers->ExceptionRecord->ExceptionCode;
	DWORD_PTR readWrite;
	DWORD_PTR virtAddr;

	printf("FILTER\n");
	printf("  Code: 0x%08X\n", code);

	// Handle EXCEPTION_ACCESS_VIOLATION
	if (code == EXCEPTION_ACCESS_VIOLATION)
	{
		// Determine whether it was a read, write, or execute and show virtual address
		readWrite = (DWORD)(pExceptionPointers->ExceptionRecord->ExceptionInformation[0]);
		virtAddr = (DWORD)(pExceptionPointers->ExceptionRecord->ExceptionInformation[1]);
		printf("  Description: ");
		switch (readWrite)
		{
		case 0:
			printf("Read Violation at address: 0x%08X\n\n", virtAddr);
			break;

		case 1:
			printf("Write Violation at address: 0x%08X\n\n", virtAddr);
			break;

		case 8:
			printf("Execute Violation at address: 0x%08X\n\n", virtAddr);
			break;
		}
		return EXCEPTION_EXECUTE_HANDLER;
	}
	else if (code == EXCEPTION_PRIV_INSTRUCTION)
	{
		virtAddr = pExceptionPointers->ContextRecord->Eip - 1;
		if (virtAddr == (DWORD_PTR)heapChunk)
		{
			printf("Memory at 0x%08X is not valid code\n", heapChunk);
			ConsolePromptPressEnter(NULL);
			return EXCEPTION_EXECUTE_HANDLER;
		}
	}

	return EXCEPTION_CONTINUE_SEARCH;
}