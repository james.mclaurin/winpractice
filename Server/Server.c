#undef UNICODE

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>

// Need to link with Ws2_32.lib
#pragma comment (lib, "Ws2_32.lib")
// #pragma comment (lib, "Mswsock.lib")

#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "27015"

#define EXIT_STRING_SIZE 5
const char EXIT_STRING[EXIT_STRING_SIZE] = { "EXIT" };

DWORD WINAPI EchoThread(LPVOID lp)
{
	DWORD iResult;
	DWORD iSendResult;
	DWORD recvbuflen = DEFAULT_BUFLEN;
	SOCKET ClientSocket = (SOCKET)lp;
	CHAR recvbuf[DEFAULT_BUFLEN];
	CHAR sendbuf[DEFAULT_BUFLEN];


	// TODO: Receive and Echo until the peer shuts down the connection

	do 
	{

		ZeroMemory(recvbuf, DEFAULT_BUFLEN);
		ZeroMemory(sendbuf, DEFAULT_BUFLEN);

	//	iResult = recv(ClientSocket, recvbuf, recvbuflen, 0);
		if (iResult > 0) 
		{

			printf("%s\n", recvbuf);

			/* TODO: Echo the buffer back to the sender */

		}
		else if (iResult == 0)
			printf("Connection closing...\n");
		else 
		{
			printf("recv failed with error: %d\n", WSAGetLastError());

			/* TODO: Socket no longer needed */

			WSACleanup();
			return 1;
		}

	} while (iResult > 0);

	return 0;
}

int __cdecl main(void)
{
	WSADATA wsaData;
	int iResult;

	SOCKET ListenSocket = INVALID_SOCKET;
	SOCKET ClientSocket = INVALID_SOCKET;

	struct addrinfo *result = NULL;
	struct addrinfo hints;

	int iSendResult;
	char recvbuf[DEFAULT_BUFLEN];
	int recvbuflen = DEFAULT_BUFLEN;

	// TODO: Initialize Winsock

	ZeroMemory(&hints, sizeof(hints));

	/* TODO: Set family, socktype, and protocol elements for addrinrfo hints */
	
	/* TODO: Resolve the server address and port */

	/* TODO: Retrieve address information */

	/* TODO: Create a SOCKET for connecting to server */

	/* TODO: Setup the TCP listening socket */

	freeaddrinfo(result);

	/* TODO: Begin Listening */

	while (TRUE)
	{
		/* TODO: Accept a client socket */

		CreateThread(NULL, 0, EchoThread, ClientSocket, 0, NULL);
	}

	// TODO: No longer need server socket

	WSACleanup();

	return 0;
}