// SemaphoreTour.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "..\commonLib\commonLib.h"

#define M_CREATE_SEMAPHORE		1
#define M_THREAD_WAIT_SEMAPHORE	2
#define M_RELEASE_SEMAPHORE		3
#define M_CLOSE_SEMAPHORE		4

LONG tokens		= 0;
volatile DWORD threads		= 0;
volatile BOOL bReleaseFlag	= FALSE;
HANDLE hSemiphore			= NULL;

static DWORD WINAPI ThreadWaitSemaphore( LPVOID lp );

int _tmain(int argc, _TCHAR* argv[])
{
	BOOL	finished = false;
	TCHAR	title[TMAX_PATH]  = {0};
	while(!finished)
	{
		_stprintf( title, _T("Semaphore Tour\n")
						  _T("Threads Waiting on semaphore: %d\n")
						  _T("Semaphores tokens available:  %d\n")
						  _T("\tBlack: Semaphore pool not created\n")
						  _T("\tGreen: Semaphore pool Created\n"), threads, tokens);

		DWORD choice = ConsolePromptMenu( TRUE,
								    /*T*/ title,
									/*1*/ _T("Create Semaphore Pool"),
									/*2*/ _T("Thread Wait on Semaphore Token"),
									/*3*/ _T("Release Semaphore Token"),
									/*4*/ _T("Close Semaphore"),NULL);
		switch(choice)
		{
			case 0:
				finished = true;
			break;

			case M_CREATE_SEMAPHORE:
				if(hSemiphore != NULL)
				{
					printf("Semiphore already Created\n");
					break;
				}
				hSemiphore = CreateSemaphore(NULL,5,5,NULL);
				if(hSemiphore != NULL)
				{
					ConsoleColor(BACKGROUND_GREEN);
					tokens = 5;
				}
			break;

			case M_THREAD_WAIT_SEMAPHORE:
				if(hSemiphore == NULL)
				{
					printf("Semiphore not created yet\n");
					break;
				}
				CreateThread(NULL,0,ThreadWaitSemaphore,NULL,0,NULL);
			break;

			case M_RELEASE_SEMAPHORE:
				if(hSemiphore == NULL)
				{
					printf("Semiphore not created yet\n");
					break;
				}
				bReleaseFlag = TRUE;
			break;

			case M_CLOSE_SEMAPHORE:
				if(hSemiphore != NULL)
				{
					CloseHandle(hSemiphore);
					ConsoleColor(0);
					hSemiphore = NULL;
					tokens = 0;
					threads = 0;
					break;
				}
				
			break;

			default:
				_tprintf(_T("\nInvalid Choice\n"));
			break;
		}

		ConsolePromptPressEnter(NULL);
	}
}

static DWORD WINAPI ThreadWaitSemaphore( LPVOID lp )
{
	InterlockedIncrement(&threads);
	WaitForSingleObject(hSemiphore, INFINITE);
	InterlockedDecrement(&threads);
	InterlockedDecrement(&tokens);
	while(!bReleaseFlag)
	{
		Sleep(250);
	}
	ReleaseSemaphore(hSemiphore,1,NULL);
	InterlockedIncrement(&tokens);
	bReleaseFlag = FALSE;
	
	return 0;
}



