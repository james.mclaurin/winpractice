#include <stdio.h>
#include "..\math\math.h" /* There is stuff to fix in your header file */

/* make sure you have your project linker properties set to link against your library */

int main(int argc, CHAR* argv[])
{
	DWORD a = 10, b = 5;
    /* Once your header file is complete, uncomment and it will compile */
    
	printf("%2d + %2d = %2d\n", a,b,Add(a,b));
	printf("%2d - %2d = %2d\n", a,b,Subtract(a,b));
	printf("%2d * %2d = %2d\n", a,b,Multiply(a,b));
	printf("%2d / %2d = %2d\n", a,b,Divide(a,b));

	return 0;
}