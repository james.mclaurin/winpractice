// GetWindowsVersion.cpp : Defines the entry point for the console application.
//
#include "..\common\common.h"

/* 
    GetWindowsVersion Lab:

    Print the windows version by querying it from the registry
    Ensure to perform error checking and report accordingly
    Ensure to close HKEY (HANDLES) when done using them;
*/

#define BUF_SIZE 16

int main(int argc, CHAR* argv[])
{
    HKEY    hkey                = NULL;
    DWORD    ret                    = 0;
    TCHAR    version[BUF_SIZE]    = {0};
    DWORD    versionSize            = BUF_SIZE * sizeof(TCHAR);
    DWORD    type                = REG_SZ;
    BOOL    success                = FALSE;
	CHAR	buffer[MAX_PATH] = { 0 };

	__try
	{
		/* Open the key HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion */
		ret = RegOpenKeyEx(
			HKEY_LOCAL_MACHINE,
			"SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion",
			0,
			KEY_READ,
			&hkey);

		if (ret != ERROR_SUCCESS)
		{
			ConsolePromptPressEnter("Failed to open registry\n");
			__leave;
		}

		/* Query the Value CurrentVersion */
		ret = RegQueryValueEx(
			hkey,
			"CurrentVersion",
			0,
			&type,
			(LPBYTE)version,
			&versionSize);

		if (ret != ERROR_SUCCESS)
		{
			ConsolePromptPressEnter("Failed to open registry\n");
			__leave;
		}

		success = TRUE;
	}
    __finally
    {
        /* if hkey is valid close the handle */
		if (hkey != NULL) RegCloseKey(hkey);

        if(success)
			sprintf(buffer, "Windows Version: %s\n", version);
        else
            sprintf(buffer, "Unable To Determine Windows Version\n");

		ConsolePromptPressEnter(buffer);
		memset(buffer, 0, MAX_PATH);
    }

    return 0;
}

