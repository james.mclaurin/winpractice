#ifndef _MATH_H
#define _MATH_H
#include <Windows.h>

/* create #defines for EXPORTING_DLL */
#ifdef EXPORTING_DLL
#define DECL_EXP_IMP __declspec(dllexport)
#else
#define DECL_EXP_IMP __declspec(dllexport)
#endif

/*
    Create for functions to export 
    all calling conventions should be __cdecl
    Functions should be Add, Subtract, Multiply, Divide
    Return a DWORD and take two parameters
*/
DECL_EXP_IMP DWORD __cdecl Add(DWORD a, DWORD b);
DECL_EXP_IMP DWORD __cdecl Subtract(DWORD a, DWORD b);
DECL_EXP_IMP DWORD __cdecl Multiply(DWORD a, DWORD b);
DECL_EXP_IMP DWORD __cdecl Divide(DWORD a, DWORD b);

#endif // _MATH_H