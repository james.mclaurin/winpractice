#define EXPORTING_DLL      /* are we exporting or not? */
#include "math.h"

/* create your functions in math.h */
DWORD __cdecl Add(DWORD a, DWORD b)
{
	DWORD retVal = 0;
	retVal = a + b;
	return retVal;
}

DWORD __cdecl Subtract(DWORD a, DWORD b)
{
	DWORD retVal = 0;
	retVal = (a) - (b);
	return retVal;
}

DWORD __cdecl Multiply(DWORD a, DWORD b)
{
	DWORD retVal = 0;
	retVal = (a) * (b);
	return retVal;
}

DWORD __cdecl Divide(DWORD a, DWORD b)
{
	DWORD retVal = 0;
	retVal = (a)/(b);
	return retVal;
}
