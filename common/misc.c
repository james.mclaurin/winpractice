#include "common.h"

LPVOID GetProcAddressWrapper(LPSTR dll, LPSTR func)
{
	HMODULE hDll;
	FARPROC	procAddr;

	printf("  Loading %s : %s\n", dll, func);
	hDll = LoadLibraryA(dll);
	if(hDll == NULL)
	{
		ReportError("    Error Loading Library", 0, TRUE);
		return NULL;
	}

	procAddr = GetProcAddress(hDll, func);
	if(procAddr == NULL)
	{
		ReportError("    Error Getting Proc Address", 0, TRUE);
		return NULL;
	}

	return procAddr;
}