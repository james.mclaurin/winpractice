#include "common.h"

VOID ConsolePromptPressEnter(LPSTR msg)
{
    CHAR    prompt[] = {"Press enter to continue"};
    LPSTR    fullPrompt = NULL;
    DWORD    msgLen;
    CHAR    response[PRESS_ENTER_SIZE] = {0};

    if(msg == NULL)
    {
        fullPrompt = prompt;
    }
    else
    {
        msgLen = strlen(msg) + strlen(prompt) + 2; // Message will have a \n and NULL term
        fullPrompt = (LPTSTR)malloc(msgLen);
        if(fullPrompt == NULL)
        {
            fullPrompt = prompt;
        }
        sprintf_s(fullPrompt, msgLen, "%s\n%s", msg, prompt);
    }

    ConsolePrompt(fullPrompt, response, PRESS_ENTER_SIZE, FALSE);

    if(msg != NULL)
    {
        free(fullPrompt);
    }

    return;
}

BOOL PrintStrings(HANDLE hOut, ...)
/* Write the messages to the output handle. Frequently hOut
will be standard out or error, but this is not required.
Use WriteConsole first, as the output will normally be the console. 
If that fails, use WriteFile.

hOut:    Handle for output file. 
... :    Variable argument list containing CHAR strings.
    The list must be terminated with NULL. */
{
    DWORD        msgLen;
    DWORD        count;
    LPCSTR        pMsg;

    va_list        pMsgList;            /* Current message string. */
    va_start    (pMsgList, hOut);    /* Start processing msgs. */

    if(hOut == NULL)
        hOut = GetStdHandle(STD_OUTPUT_HANDLE);

    while ((pMsg = va_arg (pMsgList, LPCSTR)) != NULL) 
    {
        msgLen = strlen(pMsg);
        if (!WriteConsole (hOut, pMsg, msgLen, &count, NULL) && 
            !WriteFile (hOut, pMsg, msgLen, &count, NULL)) 
        {
            va_end (pMsgList);
            return FALSE;
        }
    }
    va_end (pMsgList);
    return TRUE;    
}

BOOL PrintMsg(HANDLE hOut, LPSTR pMsg)
/* For convenience only - Single message version of PrintStrings so that
you do not have to remember the NULL arg list terminator.

hOut:    Handle of output file
pMsg:    Single message to output. */
{
    return PrintStrings (hOut, pMsg, NULL);
}

BOOL ConsolePrompt( LPSTR pPromptMsg, LPSTR pResponse, DWORD maxChar, BOOL echo)
/* Prompt the user at the console and get a response
    which can be up to MaxChar characters.

    pPromptMessage:    Message displayed to user.
    pResponse:    Programmer-supplied buffer that receives the response.
    maxChar:    Maximum size of the user buffer, measured as generic characters.
    echo:        Do not display the user's response if this flag is FALSE. */
{
    HANDLE  hIn        = INVALID_HANDLE_VALUE;
    HANDLE  hOut       = INVALID_HANDLE_VALUE;
    DWORD   charIn     = 0;
    DWORD   echoFlag   = 0;
    BOOL    success    = FALSE;

    __try
    {
        hIn = CreateFile(
                "CONIN$", 
                GENERIC_READ | GENERIC_WRITE, 
                0,
                NULL, 
                OPEN_ALWAYS, 
                FILE_ATTRIBUTE_NORMAL, 
                NULL);
        if(hIn == INVALID_HANDLE_VALUE)
        {
            ReportError("Create $CONIN Error", 0, TRUE);
            __leave;
        }

        hOut = CreateFile(
                "CONOUT$", 
                GENERIC_WRITE, 
                0,
                NULL, 
                OPEN_ALWAYS, 
                FILE_ATTRIBUTE_NORMAL, 
                NULL);
                if(hIn == INVALID_HANDLE_VALUE)
        {
            ReportError("Create $CONOUT Error", 0, TRUE);
            __leave;
        }

        /* Should the input be echoed? */
        echoFlag = echo ? ENABLE_ECHO_INPUT : 0;

        /* API "and" chain. If any test or system call fails, the
            rest of the expression is not evaluated, and the
            subsequent functions are not called. GetStdError ()
            will return the result of the failed call. */
        success =  
            SetConsoleMode (hIn, ENABLE_LINE_INPUT | echoFlag | ENABLE_PROCESSED_INPUT) &&
            SetConsoleMode (hOut, ENABLE_WRAP_AT_EOL_OUTPUT | ENABLE_PROCESSED_OUTPUT)    &&
            PrintStrings (hOut, pPromptMsg, NULL)                                        &&
            ReadConsole (hIn, pResponse, maxChar - 2, &charIn, NULL); /*-2 because of CRLF"    */                


        PrintMsg( hOut, "\n");
    }
    __finally
    {
        if(hIn != INVALID_HANDLE_VALUE)CloseHandle(hIn);
        if(hOut != INVALID_HANDLE_VALUE)CloseHandle(hOut);
        
        /* Replace the CR-LF by the null character. */
        if (success)
            pResponse [charIn - 2] = '\0';
        else
            ReportError("ConsolePrompt failure", 0, TRUE);
    }
    return success;
}

DWORD ConsolePromptMenu( BOOL clearScreen, LPSTR titleString, ...)
{
    CHAR    choice[PROMPT_MENU_SIZE]    = {0};
    DWORD    i                            = 1;
    LPSTR    msg                            = NULL;
    LPSTR    pMsg                        = NULL;
    HANDLE    hConsoleOut                    = INVALID_HANDLE_VALUE;
    DWORD    dChoice;
    va_list vList;


    va_start( vList, titleString );
    hConsoleOut = CreateFile(
                    "CONOUT$", 
                    GENERIC_READ|GENERIC_WRITE, 
                    0,
                    NULL, 
                    OPEN_ALWAYS, 
                    FILE_ATTRIBUTE_NORMAL, 
                    NULL); 

    if(clearScreen)ClearScreen();

    pMsg = va_arg(vList, LPTSTR);
    if(pMsg == NULL)
    {
        return INVALID_MENU_ENTRY;
    }
    
    if(titleString)PrintMsg(hConsoleOut, titleString);
    do{
        DWORD fullMsgSize = strlen(pMsg) + 32;
        msg = (LPSTR)calloc( fullMsgSize, sizeof(CHAR));
        if(msg == NULL) continue;
        sprintf_s(msg, fullMsgSize, "  %d: %s\n", i, pMsg);
        PrintMsg(hConsoleOut, msg);
        free(msg);
        i++;
    }while(( pMsg = va_arg(vList,LPTSTR)) != NULL);

    ConsolePrompt("  0: Exit\n\nPlease Make a Choice: ", choice, PROMPT_MENU_SIZE, TRUE);
    if(!strcmp(choice,""))
        dChoice = INVALID_MENU_ENTRY;
    else
        dChoice = atoi(choice);

    CloseHandle(hConsoleOut);

    return dChoice;
}

VOID ClearScreen()
{
    COORD                        coordScreen = { 0, 0 };     /* here's where we'll home the cursor */ 
    HANDLE                        hConsoleOut;
    DWORD                        cCharsWritten;
    CONSOLE_SCREEN_BUFFER_INFO    csbi = {0};                    /* to get buffer info */ 
     DWORD                        dwConSize;                      /* number of character cells in
                                                              the current buffer */ 
    __try
    {
        hConsoleOut = CreateFile(
                            "CONOUT$", 
                            GENERIC_READ|GENERIC_WRITE, 
                            0,
                            NULL, 
                            OPEN_ALWAYS, 
                            FILE_ATTRIBUTE_NORMAL, 
                            NULL);
        if(hConsoleOut == INVALID_HANDLE_VALUE)
        {
            ReportError("Failed To CreateFile for Console Out",0,TRUE);
            __leave;
        }

        /* get the number of character cells in the current buffer */ 
        if(!GetConsoleScreenBufferInfo( hConsoleOut, &csbi ))
        {
            ReportError( "GetConsoleScreenBufferInfo", 0, TRUE );
            __leave;
        }
        dwConSize = csbi.dwSize.X * csbi.dwSize.Y;

        /* fill the entire screen with blanks */ 
        if(!FillConsoleOutputCharacter( hConsoleOut, (TCHAR) ' ',dwConSize, coordScreen, &cCharsWritten ))
        {
            ReportError("FillConsoleOutputCharacter", 2, TRUE );
            __leave;
        }

        /* get the current text attribute */ 
        if(!GetConsoleScreenBufferInfo( hConsoleOut, &csbi ))
        {
            ReportError("GetConsoleScreenBufferInfo", 3, TRUE );
            __leave;
        }

        /* now set the buffer's attributes accordingly */
        if(!FillConsoleOutputAttribute( hConsoleOut, csbi.wAttributes,dwConSize, coordScreen, &cCharsWritten ))
        {
            ReportError("FillConsoleOutputAttribute", 3, TRUE );
            __leave;
        }

        /* put the cursor at (0, 0) */ 
        if(!SetConsoleCursorPosition( hConsoleOut, coordScreen ))
        {
            ReportError("SetConsoleCursorPosition", 3, TRUE );
            __leave;
        }
    }
    __finally
    {
        CloseHandle(hConsoleOut);
    }

     return;
}

VOID HexDump( LPVOID data, DWORD size)
{
     /* dumps size bytes of *data to stdout. Looks like:
      * [0000] 75 6E 6B 6E 6F 77 6E 20 30 FF 00 00 00 00 39 00 unknown 0.....9.
      */

     unsigned char *p = (unsigned char *)data;
     unsigned char c;
     unsigned int n;
     char bytestr[4] = {0};
     char addrstr[10] = {0};
     char hexstr[ 16*3 + 5] = {0};
     char charstr[16*1 + 5] = {0};
     for(n=1;n<=size;n++) 
    {
          if (n%16 == 1) {
                /* store address for this line */
                _snprintf(addrstr, sizeof(addrstr), "%.8x", p);//((unsigned int)p-(unsigned int)data) );
          }
                
          c = *p;
          if (isalnum(c) == 0) {
                c = '.';
          }

          /* store hex str (for left side) */
          _snprintf(bytestr, sizeof(bytestr), "%02X ", *p);
          strncat(hexstr, bytestr, sizeof(hexstr)-strlen(hexstr)-1);

          /* store char str (for right side) */
          _snprintf(bytestr, sizeof(bytestr), "%c", c);
          strncat(charstr, bytestr, sizeof(charstr)-strlen(charstr)-1);

          if(n%16 == 0) 
        { 
                /* line completed */
                printf("[%8.8s]    %-50.50s  %s\n", addrstr, hexstr, charstr);
                hexstr[0] = 0;
                charstr[0] = 0;
          } 
        else if(n%8 == 0) 
        {
                /* half line: add whitespaces */
                strncat(hexstr, "  ", sizeof(hexstr)-strlen(hexstr)-1);
                strncat(charstr, " ", sizeof(charstr)-strlen(charstr)-1);
          }
          p++; /* next byte */
     }

     if (strlen(hexstr) > 0) 
    {
          /* print rest of buffer if not empty */
          printf("[%8.8s]    %-50.50s  %s\n", addrstr, hexstr, charstr);
     }
}

VOID ConsoleColor(WORD color)
{
    HANDLE  hConsoleOut = INVALID_HANDLE_VALUE;
    WORD    newColor = color;

    newColor |= (FOREGROUND_RED|FOREGROUND_GREEN|FOREGROUND_BLUE|FOREGROUND_INTENSITY);

    hConsoleOut = CreateFile(
                            "CONOUT$", 
                            GENERIC_READ|GENERIC_WRITE, 
                            0,
                            NULL, 
                            OPEN_ALWAYS, 
                            FILE_ATTRIBUTE_NORMAL, 
                            NULL);
    if(hConsoleOut == INVALID_HANDLE_VALUE)
    {
        ReportError("Failed To CreateFile for Console Out",0,TRUE);
        return;
    }

    SetConsoleTextAttribute(hConsoleOut, newColor);
}
