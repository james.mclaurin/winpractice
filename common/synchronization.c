#include "common.h"

void SignalProcessByEvent()
{
	HANDLE hEvent = NULL;

	hEvent = OpenEvent(EVENT_MODIFY_STATE ,FALSE,SHARED_EVENT_NAME);
	if(hEvent == NULL)
	{
		ReportError("OpenEvent Error", 1, TRUE);
	}

	SetEvent(hEvent);
	CloseHandle(hEvent);
}

void WaitForProcessByEvent()
{
	HANDLE hEvent = NULL;

	printf("Waiting for signal from another process\n");

	hEvent = CreateEvent(NULL, TRUE, FALSE, SHARED_EVENT_NAME);
	if(GetLastError() == ERROR_ALREADY_EXISTS)
	{
		ReportError("Event Already Exists, server should be creating it, exiting now\n", 0, FALSE);
		CloseHandle(hEvent);
	}

	WaitForSingleObject(hEvent, INFINITE);
	CloseHandle(hEvent);
}