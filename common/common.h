#ifndef __COMMON_H
#define __COMMON_H

#include <Windows.h>
#include <stdarg.h>
#include <stdio.h>

//disable deprecated function warning
#pragma warning(disable : 4996)

typedef void* ReplaceMe_t;
#define REPLACE_ME (ReplaceMe_t)0


#define FIX_ME 0

#define MAX_OPTIONS            20                    /* Max # command line options */
#define MAX_ARG                1000                /* Max # of command line arguments */
#define MAX_COMMAND_LINE    MAX_PATH+50            /* Max size of a command line */
#define MAX_PATH_LONG        32767                /* Very long path names with \\?\ prefix. */
#define MAX_NAME            256                    /* Name length - users and groups */
#define DIRNAME_LEN            (MAX_PATH + 2)        /* The name is more fitting in situations */
#define PRESS_ENTER_SIZE    8                    /* press enter generally returns 0x0d 0x0a */
#define PROMPT_MENU_SIZE    8                    /* When asking for a number on the menu screens */
#define INVALID_MENU_ENTRY    0xFFFFFF

#define SHARED_MUTEX_NAME		"WINSYSPROG_SHARED_MUTEX_NAME"
#define SHARED_EVENT_NAME		"WINSYSPROG_SHARED_EVENT_NAME"
#define SHARED_MEMORY_NAME		"WINSYSPROG_SHARED_MEMORY_NAME"
#define SHARED_MEMORY_BUF_SIZE	256

#ifdef __cplusplus
extern "C"
{
#endif
//ReportError.cpp
VOID ReportError (LPCSTR userMessage, DWORD exitCode, BOOL printErrorMessage);
VOID ReportException (LPCSTR userMessage, DWORD exceptionCode);

//Options.cpp
DWORD Options (int argc, LPSTR argv [], LPCSTR OptStr, ...);

//Console.cpp
VOID ConsolePromptPressEnter(LPSTR msg);
BOOL PrintStrings(HANDLE hOut, ...);
BOOL PrintMsg(HANDLE hOut, LPSTR pMsg);
BOOL ConsolePrompt( LPSTR pPromptMsg, LPSTR pResponse, DWORD MaxChar, BOOL echo);
DWORD ConsolePromptMenu( BOOL clearScreen, LPSTR titleString, ...);
VOID ClearScreen();
VOID HexDump( LPVOID data, DWORD size);
VOID ConsoleColor(WORD color);

// Synchronization.cpp
VOID WaitForProcessByEvent();
VOID SignalProcessByEvent();

// Misc.cpp
LPVOID GetProcAddressWrapper(LPSTR dll, LPSTR func);
BOOL EnableDebug(BOOL bEnable);
#ifdef __cplusplus
}
#endif
#endif