#include "MathUseExplicit.h" /* There is stuff to fix in your header file */
#include "..\common\common.h"

int main(int argc, CHAR* argv[])
{
    /* Create function pointers using your function typedefs 
       Call Them Add, Sub, Mul Div
    */

	TypeAdd Add;
	TypeSub Sub;
	TypeMul Mul;
	TypeDiv Div;
    
    DWORD   a   = 10;
    DWORD   b   = 5;

	printf("loading libraries\n");
    /* Uncomment and this should compile when you have your
       typedefs and variables setup correctly
    */

	Add = (TypeAdd)GetProcAddressWrapper( "math.dll", "Add");
	Sub = (TypeSub)GetProcAddressWrapper( "math.dll", "Subtract");
	Mul = (TypeMul)GetProcAddressWrapper( "math.dll", "Multiply");
	Div = (TypeDiv)GetProcAddressWrapper( "math.dll", "Divide");
    
	
    if(!Add || !Sub || !Mul || !Div)
		return 1;
    printf("libraries loaded\n\n");
	
	printf("%2d + %2d = %2d\n", a,b,Add(a,b));
	printf("%2d - %2d = %2d\n", a,b,Sub(a,b));
	printf("%2d * %2d = %2d\n", a,b,Mul(a,b));
	printf("%2d / %2d = %2d\n", a,b,Div(a,b)); 
    
	ConsolePromptPressEnter("");
	return 0;
}

LPVOID GetProcAddressWrapper(LPSTR dll, LPSTR func)
{
	HMODULE hDll;
	FARPROC	procAddr;

	printf("  Loading %s : %s\n", dll, func);
    /* Load your library */
	hDll = LoadLibrary(dll);

    /* Get your functions address from the library */
	procAddr = GetProcAddress(hDll, func);

	return procAddr;
}