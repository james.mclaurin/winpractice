#ifndef _MATH_USE_EXPLICIT_H
#define _MATH_USE_EXPLICIT_H
#include <Windows.h>

/* typedef your function types */
typedef DWORD (__cdecl *TypeAdd)(DWORD a, DWORD b);
typedef DWORD (__cdecl *TypeSub)(DWORD a, DWORD b);
typedef DWORD (__cdecl *TypeMul)(DWORD a, DWORD b);
typedef DWORD (__cdecl *TypeDiv)(DWORD a, DWORD b);

#endif