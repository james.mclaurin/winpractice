#include "ProcessTour.h"

BOOL				gIsProcessCreated		= FALSE;
PROCESS_INFORMATION gProcessInfo			= {0};
TCHAR				gProcessName[MAX_PATH]	= {0};

int main(int argc, CHAR* argv[])
{
    DWORD   choice = 0;

	do
	{
        if(gIsProcessCreated)
        {
            PrintProcessInfo();
        }
		choice = ConsolePromptMenu( FALSE,
								    /*T*/ "Process Tour Menu:\n",
									/*1*/ "Print Process List",
									/*2*/ "Create Process",
									/*3*/ "Wait on Process To Finish",
									/*4*/ "Terminate Process",
                                    NULL);
		switch(choice)
		{
			case 0:
				
			break;

			case PRINT_PROCESS_LIST:
				ProcessPrintList();
			break;

			case CREATE_PROCESS:
				ProcessCreate();
			break;

			case WAIT_ON_PROCESS:
				ProcessWait();
			break;

			case TERMINATE_PROCESS:
				ProcessTerminate();
			break;

			default:
				printf("Invalid Choice");
			break;
		}

		ConsolePromptPressEnter(NULL);
        ClearScreen();
	}while(choice != 0);

	return 0;
}

/* Use ToolHelp32Snapshot */
VOID ProcessPrintList()
{
	HANDLE hProcessSnap;
	PROCESSENTRY32 pe32;

	// Take a snapshot of all processes in the system.
	hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (hProcessSnap == INVALID_HANDLE_VALUE)
	{
		ReportError("CreateToolhelp32Snapshot (of processes)", 0, TRUE);
		return;
	}

	// Set the size of the PROCESSENTRY32 structure before using it.
	pe32.dwSize = sizeof(PROCESSENTRY32);

	// Retrieve information about the first process from hProcessSnap
    // Store it in pe32
	if (!Process32First(hProcessSnap, &pe32))
	{
		ReportError("Process32First", 0, TRUE);
		CloseHandle(hProcessSnap);     // Must clean up the snapshot object!
		return;
	}

	// Now walk the snapshot of processes and display pid and name
	do
	{
		printf("\n%6d\t%s", pe32.th32ProcessID, pe32.szExeFile);
	} while (Process32Next(hProcessSnap, &pe32));

	printf("\n\n");

	CloseHandle( hProcessSnap );
	return;

}

VOID ProcessCreate()
{
	/* Create a Process, only one from this process can be created at a time */
    /* PROCESS_INFO is global use gProcessInfo */
	PROCESS_INFORMATION pi = { 0 };
	STARTUPINFO			si					= {0};
	TCHAR				szCmdline[MAX_PATH]	= {0};

	if(gIsProcessCreated)
	{
		printf("Process is already Created:\n %4d\t%s\n", gProcessInfo.dwProcessId, gProcessName);
		return;
	}

	// Get Process To Start
	ConsolePrompt("Enter Process To Create: ", szCmdline, MAX_PATH, TRUE);
	
	si.cb = sizeof(si);

	if (!CreateProcess(NULL,
		szCmdline,
		NULL,
		NULL,
		FALSE,
		FALSE,
		NULL,
		NULL,
		&si,
		&pi))
	{
		ReportError("CreateProcess", 0, TRUE);
		return;
	}

	return;
}

VOID ProcessWait()
{
	DWORD   exitCode;
	HANDLE  hProc;
	CHAR	_pid[16] = { 0 };
	DWORD	pid;

	ConsolePrompt("Enter PID: ", _pid, 16, TRUE);
	pid = atoi(_pid);


	hProc = OpenProcess(PROCESS_QUERY_INFORMATION | SYNCHRONIZE,
		FALSE,
		pid);
	if (hProc == NULL)
	{
		ReportError("Error Opening Process", 0, TRUE);
		return;
	}

	switch (WaitForSingleObject(hProc, 10000))
	{
	case WAIT_OBJECT_0:
		GetExitCodeProcess(hProc, &exitCode);
		printf("Process Exited: %d\n", exitCode);
		break;

	case WAIT_TIMEOUT:
		printf("Process did not exit within timeout period\n");
		break;

	default:
		ReportError("Error Waiting on Process", 0, TRUE);
		break;
	}

	return;
}

VOID ProcessTerminate()
{
	HANDLE hProc;
	CHAR	_pid[16] = { 0 };
	DWORD	pid;

	ConsolePrompt("Enter PID: ", _pid, 16, TRUE);
	pid = atoi(_pid);

	hProc = OpenProcess(PROCESS_TERMINATE, FALSE, pid);
	if (hProc == NULL)
	{
		ReportError("Error Opening Process", 0, TRUE);
		return;
	}

	if (!TerminateProcess(hProc, TERMINATE_PROCESS))
	{
		ReportError("Error Terminating Process", 0, TRUE);
		return;
	}

	printf("Process Terminated\n");
	return;
}

VOID PrintProcessInfo()
{
	FILETIME	createTime, exitTime, kernelTime, userTime;
	FILETIME	_createTime, _exitTime, _kernelTime, _userTime;
	SYSTEMTIME	sysCreateTime;
	HANDLE		hProc;

	CHAR	_pid[16] = { 0 };
	DWORD	pid;

	ConsolePrompt("Enter PID: ", _pid, 16, TRUE);
	pid = atoi(_pid);

	hProc = OpenProcess(PROCESS_QUERY_INFORMATION | SYNCHRONIZE,
		FALSE,
		pid);
	if (hProc == NULL)
	{
		ReportError("Error Opening Process", 0, TRUE);
		return;
	}

	GetProcessTimes(hProc, &createTime, &exitTime, &kernelTime, &userTime);

	FileTimeToLocalFileTime(&createTime, &_createTime);
	FileTimeToSystemTime(&_createTime, &sysCreateTime);


	printf("[%d] Created:\t%02d\\%02d\\%04d  %02d:%02d:%02d\n\n",
		pid,
		sysCreateTime.wMonth, sysCreateTime.wDay, sysCreateTime.wYear,
		sysCreateTime.wHour, sysCreateTime.wMinute, sysCreateTime.wSecond);

	return;
}