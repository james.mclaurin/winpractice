#include "..\common\common.h"

#include <tlhelp32.h>

#define PRINT_PROCESS_LIST		1
#define CREATE_PROCESS			2
#define WAIT_ON_PROCESS			3
#define TERMINATE_PROCESS		4

VOID ProcessPrintList();
VOID ProcessCreate();
VOID ProcessWait();
VOID ProcessTerminate();
VOID PrintProcessInfo();