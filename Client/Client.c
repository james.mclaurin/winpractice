#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>

#include "..\common\common.h"


// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")


#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "27015"

#define EXIT_STRING_SIZE 5
const char EXIT_STRING[EXIT_STRING_SIZE] = { "EXIT" };

int __cdecl main(int argc, char **argv)
{
	WSADATA wsaData = { 0 };
	SOCKET ConnectSocket = INVALID_SOCKET;
	struct addrinfo *result = NULL,
		*ptr = NULL,
		hints;
	DWORD iResult = 0;

	// Validate the parameters
	if (argc != 2) 
	{
		printf("usage: %s server-name\n", argv[0]);
		return 1;
	}

	// Todo: Initialize Winsock

	ZeroMemory(&hints, sizeof(hints));

	/* TODO: Set family, socktype, and protocol elements for addrinrfo hints */

	/* TODO: Resolve the server address and port */

	/* TODO: Retrieve address information */

	// Attempt to connect to an address until one succeeds
	for (ptr = result; ptr != NULL;ptr = ptr->ai_next) 
	{

		// TODO: Create a SOCKET for connecting to server

		// TODO: Connect to server.
		break;
	}

	freeaddrinfo(result);

	if (ConnectSocket == INVALID_SOCKET) 
	{
		printf("Unable to connect to server!\n");
		WSACleanup();
		return 1;
	}

	char sendbuf[DEFAULT_BUFLEN];
	char recvbuf[DEFAULT_BUFLEN];
	int recvbuflen;
	do
	{
		ZeroMemory(sendbuf, DEFAULT_BUFLEN);
		ZeroMemory(recvbuf, DEFAULT_BUFLEN);
		recvbuflen = DEFAULT_BUFLEN;

		ConsolePrompt(">> ", sendbuf, DEFAULT_BUFLEN, TRUE);

		// Todo: Send an initial buffer

		// Todo: Receive until the peer closes the connection
	} while (strnicmp(recvbuf, EXIT_STRING, EXIT_STRING_SIZE) != 0 || iResult <= 0);

	// Todo: shutdown the connection since no more data will be sent

	// cleanup
	closesocket(ConnectSocket);
	WSACleanup();

	return 0;
}