#include "..\common\common.h"

int main(int argc, CHAR* argv[])
{

	HANDLE	hFileMap						= NULL;
	LPTSTR	pMemBase						= NULL;
	TCHAR	message[SHARED_MEMORY_BUF_SIZE] = {0};

	__try
	{	
		/* 
            Create a named memory map using the paging file 
            use the defined name SHARED_MEMORY_NAME
            create the memory pool with the defined size SHARED_MEMORY_BUF_SIZE
        */
		hFileMap = CreateFileMapping(
			NULL,
			NULL,
			PAGE_READWRITE,
			0,
			SHARED_MEMORY_BUF_SIZE,
			SHARED_MEMORY_NAME);

		/* Map a view of the memory, in this case all of it */
		pMemBase = (LPTSTR)MapViewOfFile(
			hFileMap,
			FILE_MAP_ALL_ACCESS,
			0,
			0,
			SHARED_MEMORY_BUF_SIZE);

		// Get a message from the user to share
		ConsolePrompt("Enter a message to pass to the other process: ",
					  pMemBase,
					  SHARED_MEMORY_BUF_SIZE,
					  TRUE);

		// Ready for another process to map and receive message
        // Why can't we just close before the client is ready?
		WaitForProcessByEvent();

	}
	__finally
	{
        /* cleanup */
		if (pMemBase)UnmapViewOfFile(pMemBase);
		if (hFileMap)CloseHandle(hFileMap);
	}
 
	return 0;
}

