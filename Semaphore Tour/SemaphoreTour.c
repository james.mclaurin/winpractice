#include "..\common\common.h"

#define M_CREATE_SEMAPHORE		1
#define M_THREAD_WAIT_SEMAPHORE	2
#define M_RELEASE_SEMAPHORE		3
#define M_CLOSE_SEMAPHORE		4

LONG            tokens		    = 0;
volatile DWORD  threads		    = 0;
volatile BOOL   bReleaseFlag	= FALSE;
HANDLE          hSemaphore		= NULL;
HANDLE          hThreadEvent    = NULL;

static DWORD WINAPI ThreadWaitSemaphore( LPVOID lp );

int main(int argc, CHAR* argv[])
{
	DWORD   choice = 0;
	TCHAR	title[MAX_PATH]  = {0};

    hThreadEvent = CreateEvent(NULL, FALSE, TRUE, NULL);
	do
	{
		sprintf( title, "Semaphore Tour\n"
						"Threads Waiting on semaphore: %d\n"
						"Semaphores tokens available:  %d\n"
						"\tBlack: Semaphore pool not created\n"
						"\tGreen: Semaphore pool Created\n"
						"\t Blue: Thread Waiting on Semaphore\n",
						threads, tokens);

		choice = ConsolePromptMenu( TRUE,
									/*T*/ title,
									/*1*/ "Create Semaphore Pool",
									/*2*/ "Thread Wait on Semaphore Token",
									/*3*/ "Release Semaphore Token",
									/*4*/ "Close Semaphore",
									NULL);
		switch(choice)
		{
			case 0:
				
			break;

			case M_CREATE_SEMAPHORE:
				if(hSemaphore != NULL)
				{
					ConsolePromptPressEnter("Semaphore already Created\n");
					break;
				}
                /* TODO: Create a semaphore pool of five tokens, all available to wait on */
				
				if(hSemaphore != NULL)
				{
					ConsoleColor(BACKGROUND_GREEN);
					tokens = 5;
				}
			break;

			case M_THREAD_WAIT_SEMAPHORE:
				if(hSemaphore == NULL)
				{
					ConsolePromptPressEnter("Semaphore not created yet\n");
					break;
				}
				if (tokens == 0)
				{
					ConsoleColor(BACKGROUND_BLUE);
				}
				CreateThread(NULL,0,ThreadWaitSemaphore,NULL,0,NULL);
			break;

			case M_RELEASE_SEMAPHORE:
				if(hSemaphore == NULL)
				{
					ConsolePromptPressEnter("Semaphore not created yet\n");
					break;
				}
				bReleaseFlag = TRUE;
			break;

			case M_CLOSE_SEMAPHORE:
				if(hSemaphore != NULL)
				{
                    /* TODO: Close semaphore pool handle */
					
                    /* TODO: Set semaphore handle to NULL */

					ConsoleColor(0);
					hSemaphore = NULL;
					tokens = 0;
					threads = 0;
					break;
				}
				
			break;

			default:
				ConsolePromptPressEnter("\nInvalid Choice\n");
			break;
		}
        switch(WaitForSingleObject(hThreadEvent, 250))
        {
            case WAIT_OBJECT_0:

            break;
        }
	}while(choice != 0);
}

/* bReleaseFlag could be much better done with an event
   extra credit, change the lab to use events instead of a bool
*/
static DWORD WINAPI ThreadWaitSemaphore( LPVOID lp )
{
	/* Increment global thread count: threads */
	InterlockedIncrement(&threads);

    /* Don't worry about this */
    SetEvent(hThreadEvent);

    /* TODO: Wait for a semiphore from the pool */
	
    /* TODO: Once returned Decrement threads waiting: threads
                           Decrement tokens, one has been retrieved */


    /* wait for token to  be released from menu */
	while(!bReleaseFlag)
	{
		Sleep(250);
	}

    /* TODO: Release one token from the semaphore pool */

    /* TODO: Increment our token count */

    /* TODO: reset flag to FALSE for next thread */
	
	return 0;
}



