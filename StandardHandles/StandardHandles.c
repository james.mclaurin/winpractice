#include "../common/common.h"
#include "StandardHandles.h"

int main(int argc, CHAR* argv[])
{
    DWORD   choice              = INVALID_MENU_ENTRY;
    HANDLE  hFile               = INVALID_HANDLE_VALUE;
    HANDLE  hConOut             = INVALID_HANDLE_VALUE;

    /* Creates a File to use during lab */
    hFile = CreateFile(
        STANDARD_FILENAME,
        GENERIC_WRITE,
        FILE_SHARE_READ | FILE_SHARE_WRITE,
        NULL,
        CREATE_ALWAYS,
        FILE_ATTRIBUTE_NORMAL,
        NULL);
    if(hFile == INVALID_HANDLE_VALUE)
    {
        printf("Error creating file: %d\n", GetLastError());
        return 0;
    }

    /* TODO:
        Get HANDLE to $CONOUT assign to hConOut
    */

    /* if failure */
	if(hConOut == INVALID_HANDLE_VALUE)
	{
		printf("Error Getting CONOUT$: %d\n", GetLastError());
		CloseHandle(hFile);
        DeleteFile(STANDARD_FILENAME);
		return 0;
	}

    do
    {

        choice = ConsolePromptMenu( TRUE,                             // Clear Screen
                                    "Choose An Option:\n",            // Title
                                    "Set STD_OUT to File",            // Option 1
                                    "Set STD_OUT to CONOUT$",         // Option 2
                                    "WriteConsole STD_OUTPUT_HANDLE", // Option 3
                                    "WriteFile    STD_OUTPUT_HANDLE", // Option 4
                                    "WriteConsole CONOUT$",           // Option 5
                                    "WriteFile    CONOUT$",           // Option 6
                                    "printf",                         // Option 7
                                    NULL);                            // Must be NULL
        
        switch(choice)
        {
            // Finished
            case 0:
                  CloseHandle(hFile);
                  DeleteFile(STANDARD_FILENAME);
            break;

            // Options
            case STANDARD_SET_STDOUT_FILE:
                StandardSetStdOut(hFile);
            break;

            case STANDARD_SET_STDOUT_CONOUT:
               StandardSetStdOut(hConOut);
            break;

            case STANDARD_WRITE_CONSOLE_STD_HANDLE:
                StandardWriteConsole(GetStdHandle(STD_OUTPUT_HANDLE));
            break;

            case STANDARD_WRITE_FILE_STD_HANDLE:
                StandardWriteFile(GetStdHandle(STD_OUTPUT_HANDLE));
            break;

            case STANDARD_WRITE_CONSOLE_CONOUT:
                StandardWriteConsole(hConOut);
            break;

            case STANDARD_WRITE_FILE_CONOUT:
                StandardWriteFile(hConOut);
            break;

            case STANDARD_PRINTF:
                StandardPrintf();
            break;

            // Unknown
            case INVALID_MENU_ENTRY:
            default:
                ConsolePromptPressEnter("Invalid Choice");
            break;

        }
        FlushFileBuffers(hFile);
    }while(choice != 0);

    return 0;
}

/* TODO:
    Set this programs standard out handle to hOut
*/
BOOL StandardSetStdOut(HANDLE hOut)
{
    
    ConsolePromptPressEnter("SUCCESS\n");
    return TRUE;
}

/* TODO:
    Write Message to Console using hOut
*/
BOOL StandardWriteConsole(HANDLE hOut)
{
    CHAR  msg[]         = {"StandardWriteConsole\n"};
    DWORD msgLen        = strlen(msg);
    CHAR error[200]     = {0};
    DWORD bytesWritten;

    /* Validate hOut is valid*/
    
    /* Write to the Console msg using hOut */
    

    ConsolePromptPressEnter("SUCCESS\n");
    return TRUE;
}

/* TODO:
    Write message to file using hOut
*/
BOOL StandardWriteFile(HANDLE hOut)
{
    CHAR  msg[]         = {"StandardWriteFile\n"};
    DWORD msgLen        = strlen(msg);
    CHAR error[200]     = {0};
    DWORD bytesWritten;

    /* Validate hOut */
    
    /* Write msg to file using hOut */
    


    ConsolePromptPressEnter("SUCCESS\n");
    return TRUE; 
}

BOOL StandardPrintf()
{
    printf("Standard Printf\n");
    ConsolePromptPressEnter(NULL);
    return TRUE;
}