#ifndef _STANDARD_HANDLES
#define _STANDARD_HANDLES

#include <Windows.h>

#define STANDARD_FILENAME "StandardHandlesTestFile.txt"

#define STANDARD_SET_STDOUT_FILE            1
#define STANDARD_SET_STDOUT_CONOUT          2
#define STANDARD_WRITE_CONSOLE_STD_HANDLE   3
#define STANDARD_WRITE_FILE_STD_HANDLE      4
#define STANDARD_WRITE_CONSOLE_CONOUT       5
#define STANDARD_WRITE_FILE_CONOUT          6
#define STANDARD_PRINTF                     7

BOOL StandardSetStdOut(HANDLE hOut);
BOOL StandardWriteConsole(HANDLE hOut);
BOOL StandardWriteFile(HANDLE hOut);
BOOL StandardPrintf();

#endif